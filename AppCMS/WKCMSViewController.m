//
//  WKCMSViewController.m
//  AppCMS
//
//  Created by 長島 伸光 on 2016/03/19.
//  Copyright © 2016年 MEAS. All rights reserved.
//

#import "WKCMSViewController.h"
#import "StorageManager.h"
#import "DownloadManager.h"

@interface WKCMSViewController () {
    NSString *launchURL_;
    UISwipeGestureRecognizer *swipeLeft_;
    UISwipeGestureRecognizer *swipeRight_;
    
    UIViewController *splashViewController_;
}

@end

@implementation WKCMSViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    reloading_ = NO;

    NSString *jScriptFormat = @"var meta = document.createElement('meta'); \
    meta.name = 'viewport'; \
    meta.content = 'width=device-width, initial-scale=%@, maximum-scale=%@, user-scalable=no'; \
    var head = document.getElementsByTagName('head')[0];\
    head.appendChild(meta);";
    NSString *jScript = [NSString stringWithFormat:jScriptFormat, [App getConfigSetting:@"initial_scale_css"], [App getConfigSetting:@"maximum_scale_css"]];
    
    WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    WKUserContentController *wkUController = [[WKUserContentController alloc] init];
    [wkUController addUserScript:wkUScript];

    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    config.allowsInlineMediaPlayback = YES;
    config.userContentController = wkUController;
    if(![App isUnderIOS9]) {
        config.allowsPictureInPictureMediaPlayback = YES;
        config.allowsAirPlayForMediaPlayback = YES;
    }
    webView_ = [[WKWebView alloc] initWithFrame:self.view.frame configuration:config];
    [self.view addSubview:webView_];
    //AutoLayout
    [self setAutoLayout];
    
    webView_.UIDelegate = self;
    webView_.navigationDelegate = self;
    
    swipeLeft_ = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft)];
    swipeLeft_.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeRight_ = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)];
    swipeRight_.direction = UISwipeGestureRecognizerDirectionRight;
    
    super.reload_ = YES;
    force_ = NO;
    for (id subview in webView_.subviews) {
        if ([[subview class] isSubclassOfClass: [UIScrollView class]]) {
            ((UIScrollView *)subview).bounces = NO;
        }
    }
    [webView_ addGestureRecognizer:swipeLeft_];
    [webView_ addGestureRecognizer:swipeRight_];
    
    //
    //初回起動時のみ表示する
    if(!self.noSplash_) {
        splashView_ = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splash"]];
        CGRect bouns = [UIScreen mainScreen].bounds;
        splashView_.frame = CGRectMake(0, 0, bouns.size.width, bouns.size.height);
        splashView_.contentMode = UIViewContentModeScaleAspectFit;
        [self.view addSubview:splashView_];
    }
    //
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSString *debug = @"false";
#ifdef DEBUG
    debug = @"true";
#endif
    [self evaluatingJavaScript:[NSString stringWithFormat:@"setIds('%@', '%@', '%@', '%@', '%@', '%@');",
                              [App getDeviceId], [App getUniqueId], @"iOS", [App iOSVerstion], [App applicationVersion], debug
                              ] stopProgress:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [webView_ stopLoading];
    [super viewDidDisappear:animated];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setAutoLayout
{
    [webView_ setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(webView_);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[webView_]|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:viewsDictionary]
     ];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[webView_]|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:viewsDictionary]
     ];
    [self.view layoutIfNeeded];
    
}

- (void)swipeLeft
{
    [webView_ evaluateJavaScript:@"swipeLeft();" completionHandler:^(id obj, NSError *error) {
    }];
}

- (void)swipeRight
{
    [webView_ evaluateJavaScript:@"swipeRight();" completionHandler:^(id obj, NSError *error) {
    }];
}

/**
 リロードボタン
 **/
- (void)reloadView
{
    //ニュース記事の更新
    DownloadManager *dm = [DownloadManager buildManager];
    //ui.zipを再ダウンロード
    [App removeSetting:UI_LASTUPDATE];
    //
    dm.initilaLock_ = NO;

    force_ = YES;
    if(!dm.initilaLock_) {
        //プログレス表示
//        [App displayProgress];
        super.reload_ = YES;
        [self startHttpServer:0 reRequest:NO isLaunch:YES];
    }else{
        NSLog(@"now initial process !!");
        if(![App isSleepingOrBackground]) {
            NSString *debug = @"false";
#ifdef DEBUG
            debug = @"true";
#endif
            [self evaluatingJavaScript:[NSString stringWithFormat:@"setIds('%@', '%@', '%@', '%@', '%@', '%@');",
                                        [App getDeviceId], [App getUniqueId], @"iOS", [App iOSVerstion], [App applicationVersion], debug
                                        ] stopProgress:YES];
        }
        force_ = NO;
    }
}

- (void)startHttpServerAndReload:(int)retry
{
    super.reload_ = YES;
    [self startHttpServer:retry];
}

- (void)startHttpServer:(int)retry
{
    [self startHttpServer:retry reRequest:YES];
}

- (void)startHttpServer:(int)retry reRequest:(BOOL)reRequest
{
    [self startHttpServer:retry reRequest:reRequest isLaunch:NO];
}

- (void)startHttpServer:(int)retry reRequest:(BOOL)reRequest isLaunch:(BOOL)isLaunch
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if(httpServer_) {
            [self stopHttpServer];
        }
        if(!httpServer_ || ![httpServer_ isRunning]) {
            int port = SERVER_PORT+retry;
            httpServer_ = [[HTTPServer alloc] init];
            httpServer_.port = port;
            httpServer_.documentRoot = [App documentRoot];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{ //停止待ち
                NSError *error;
                if(![httpServer_ start:&error]) {
                    if(retry < 1000) {
                        NSLog(@"already open port %d", port);
                        [self startHttpServer:retry+1 reRequest:reRequest];
                    }else{
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"ERROR"
                                                                    message:[error localizedDescription]
                                                             preferredStyle:UIAlertControllerStyleAlert];
                        [alert addAction:[UIAlertAction actionWithTitle:@"CLOSE"
                                                                  style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction *action) {
                                                                }]
                         ];
                        [self presentViewController:alert animated:YES completion:^{
                        }];
                    
                    }
                }else{
                    NSLog(@"open port %d", port);
                    if(isLaunch) {
                        [self downloadProcess];
                    }else{
                        [self reloadTop:YES];
                    }
                }
            });
        }
    });
}

- (void)downloadProcess
{
    // スプラッシュON待機
    if(!self.noSplash_) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onSplachView:)
                                                     name:SPLASH_START_NOTIFICATION object:nil];
    }
    DownloadManager *dm = [DownloadManager buildManager];
    ////推奨
//    [dm downloadUIPackage:^(BOOL sccess) {
//        if(sccess) {
//            __block id observer =
//            [[NSNotificationCenter defaultCenter] addObserverForName:FORGRAND_END_NOTIFICATION object:nil queue:nil usingBlock:^(NSNotification *note) {
//                [[NSNotificationCenter defaultCenter] removeObserver:observer];
//                [App hiddenProgress];
//            }];
//            [self reloadTop:YES];
//        }else{
//            NSString *lastUpdate = (NSString*)[App getSetting:UI_LASTUPDATE];
//            if(!lastUpdate) { //一度もダウンロードしていない
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [dm initialDownloadError:^{
//                        [self downloadProcess];
//                    }];
//                });
//            }else{
//                [self reloadTop:NO];
//                [App hiddenProgress];
//            }
//        }
//        // スプラッシュOFF
//        [[NSNotificationCenter defaultCenter] postNotificationName:SPLASH_END_NOTIFICATION object:nil];
//    }];
    
    //非推奨
    [dm initialDownload:^(BOOL sccess) {
        if(sccess) {
            __block id observer =
            [[NSNotificationCenter defaultCenter] addObserverForName:FORGRAND_END_NOTIFICATION object:nil queue:nil usingBlock:^(NSNotification *note) {
                [[NSNotificationCenter defaultCenter] removeObserver:observer];
                [App hiddenProgress];
            }];
            [self reloadTop:YES];
        }else{
            [self reloadTop:NO];
            [App hiddenProgress];
        }
        // スプラッシュOFF
        [[NSNotificationCenter defaultCenter] postNotificationName:SPLASH_END_NOTIFICATION object:nil];
        ////
    }];
}

- (void)onSplachView:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SPLASH_START_NOTIFICATION object:nil];
    if(!self.noSplash_) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CMSMain" bundle:nil];
        if(!splashViewController_) {
            splashViewController_ = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
            [self presentViewController:splashViewController_ animated:NO completion:^{
            }];
        }
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(offSplachView:)
                                                 name:SPLASH_END_NOTIFICATION object:nil];
}

- (void)offSplachView:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SPLASH_END_NOTIFICATION object:nil];
    if(splashViewController_) {
        if(splashView_) {
            [splashViewController_ dismissViewControllerAnimated:NO
                                                      completion:^{
                                                          //
                                                      }];
        }else{
            [splashViewController_ dismissViewControllerAnimated:YES
                                                      completion:^{
                                                          //
                                                      }];
        }
        splashViewController_ = nil;
    }
    [self removeInitalImage];
}

+ (NSString*)customUserAgent
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        return [NSString stringWithFormat:CUSTOM_UA, @"iphone"];
    }
    return [NSString stringWithFormat:CUSTOM_UA, @"ipad"];
}

- (void)stopHttpServer
{
    [httpServer_ stop];
    httpServer_.port = -1;
}

- (NSDictionary*)parseQuery:(NSString*)query
{
    NSMutableDictionary *result = [[NSMutableDictionary alloc] initWithCapacity:0];
    NSArray *params = [query componentsSeparatedByString:@"&"];
    for(NSString *param in params) {
        NSArray *keyvalue = [param componentsSeparatedByString:@"="];
        NSString *key = [keyvalue objectAtIndex:0];
        NSString *value = [keyvalue objectAtIndex:1];
        [result setValue:value forKey:key];
    }
    return result;
}

- (NSInteger)getPort
{
    return httpServer_.port;
}

- (WKWebView*)getWebView
{
    return webView_;
}

- (void)lauchWebView:(NSString*)url
{
    launchURL_ = [NSString stringWithString:url];
    UIAlertController *alert = nil;
    alert = [UIAlertController alertControllerWithTitle:@"ブラウザ起動"
                                                message:@"Webブラウザを起動します。よろしいですか？"
                                         preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"キャンセル"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction *action) {
                                                launchURL_ = nil;
                                            }]
     ];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction *action) {
                                                [self lauchBrowser];
                                            }]
     ];
    [self presentViewController:alert animated:YES completion:^{
    }];
}

- (void)lauchBrowser
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:launchURL_]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:launchURL_]];
    }
    launchURL_ = nil;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if([App isiPhone]) {
        return UIInterfaceOrientationMaskPortrait;
    }
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

#pragma mark NotificationBroadcastDelegate

- (void) notificatoinRemote:(NSDictionary*)userInfo
{
    NSError *error=nil;
    NSData *data=[NSJSONSerialization dataWithJSONObject:userInfo options:2 error:&error];
    NSString *jsonstr=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    [self evaluatingJavaScript:[NSString stringWithFormat:@"nativeBridge__.remoteNotification('%@')", jsonstr]];
}

- (void) notificatoinLocal:(NSDictionary*)userInfo
{
    NSError *error=nil;
    NSData *data=[NSJSONSerialization dataWithJSONObject:userInfo options:2 error:&error];
    NSString *jsonstr=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    [self evaluatingJavaScript:[NSString stringWithFormat:@"nativeBridge__.localNotification('%@')", jsonstr]];
}

@end
