//
//  WKCMSViewController.h
//  AppCMS
//
//  Created by 長島 伸光 on 2016/03/19.
//  Copyright © 2016年 MEAS. All rights reserved.
//
#define SERVER_PORT 10080
#define CUSTOM_UA @"ios webapp wkwebview %@"

#import <UIKit/UIKit.h>
#import "CMSAppDelegate.h"
#import "WKBaseViewController.h"

@interface WKCMSViewController : WKBaseViewController <NotificationBroadcastDelegate> {
}

- (void)startHttpServerAndReload:(int)retry;
- (void)startHttpServer:(int)retry;
- (void)startHttpServer:(int)retry reRequest:(BOOL)reRequest;
- (void)startHttpServer:(int)retry reRequest:(BOOL)reRequest isLaunch:(BOOL)isLaunch;
- (void)stopHttpServer;
- (WKWebView*)getWebView;

@end
