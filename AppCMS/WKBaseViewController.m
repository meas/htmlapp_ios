//
//  WKBaseViewController.m
//  AppCMS
//
//  Created by 長島 伸光 on 2016/12/30.
//  Copyright © 2016年 MEAS. All rights reserved.
//

#import "CMSAppDelegate.h"
#import "StorageManager.h"
#import "DownloadManager.h"
#import "CameraViewController.h"
#import "WKBaseViewController.h"
#import "CommonWebViewController.h"
#import "FMDatabase.h"
#import "WKCMSViewController.h"

@interface WKBaseViewController ()

@end

@implementation WKBaseViewController

@synthesize reload_, webView_;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) evaluatingJavaScript:(NSString*)script
{
    [self evaluatingJavaScript:script stopProgress:NO];
}

- (void) evaluatingJavaScript:(NSString*)script stopProgress:(BOOL)stopProgress
{
    script = [script stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    if(webView_.loading) {
        [self hasDocument:^(BOOL result) {
            if(result) {
                dispatch_async(dispatch_get_main_queue(), ^{ //即実行
                    //                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 500.0 * NSEC_PER_MSEC), dispatch_get_main_queue(), ^{
                    [self evaluatingJavaScript:script];
                });
            }
        }];
    }else{
        if(![App isSleepingOrBackground]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [webView_ evaluateJavaScript:script completionHandler:^(id obj, NSError *error) {
                    if(stopProgress) {
                        [self endSetId];
                    }
                }];
            });
        }
    }
}

-(void)endSetId
{
    dispatch_async(dispatch_get_main_queue(), ^{ //setIdの完了を待たない
        //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 500.0 * NSEC_PER_MSEC), dispatch_get_main_queue(), ^{ //setIdの完了を500msほど待つ
        [[NSNotificationCenter defaultCenter] postNotificationName:FORGRAND_END_NOTIFICATION object:nil];
    });
}

- (void)hasDocument:(void(^ __nullable)(BOOL result)) callback
{
    [webView_ evaluateJavaScript:@"document.body.innerHTML" completionHandler:^(id obj, NSError *error) {
        if(obj && [obj isKindOfClass:NSString.class] && ((NSString*)obj).length > 10) {
            callback( YES );
        }else{
            callback( NO );
        }
        
    }];
}

-(void)loadRequest:(NSURL*)url
{
    NSURL *rootUrl = [NSURL URLWithString:rootUrlString([self getPort])];
    
    double waitSec;
    
    if ([url.path isEqualToString:rootUrl.path])
        waitSec = 0.25;
    else
        waitSec = 0.0;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, waitSec * NSEC_PER_SEC), dispatch_get_main_queue(), ^{ //httpサーバ起動待ち
        NSURLRequest *request = [NSURLRequest requestWithURL:url
                                                 cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                             timeoutInterval:60.0];
        
        if(![App isUnderIOS9]) {
            webView_.allowsLinkPreview = YES;
        }
        webView_.allowsBackForwardNavigationGestures = YES;
        [webView_ loadRequest:request];
    });
}

- (void)reloadTop:(BOOL)reload
{
    if(reload) {
#ifdef DEBUG
        NSLog(@"----------------------------------------------------------!");
#endif
        if(!reloading_) {
            reloading_ = YES; //リロードボタン無効化
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{ //httpサーバ起動待ち
                //             NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://127.0.0.1:%d/index.html", httpServer_.port]];
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://localhost:%d/index.html", httpServer_.port]];
                //             NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://localhost:%d/index.html", httpServer_.port]];
                NSURLRequest *request = [NSURLRequest requestWithURL:url
                                                         cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                     timeoutInterval:60.0];
                if(![App isUnderIOS9]) {
                    webView_.allowsLinkPreview = YES;
                }
                webView_.allowsBackForwardNavigationGestures = YES;
                [webView_ loadRequest:request];
#ifdef DEBUG
                NSLog(@"++++++++++++++++++++++++++++++++++++++++++++++++++++++++!");
#endif
                [self reloadEnable];
            });
        }
    }else{
        if(!reloading_) {
            reloading_ = YES; //リロードボタン無効化
#ifdef DEBUG
            NSLog(@"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!");
#endif
            [self hasDocument:^(BOOL result) {
                if(result) {
                    if(force_) {
                        NSString *debug = @"false";
#ifdef DEBUG
                        debug = @"true";
#endif
                        [self evaluatingJavaScript:[NSString stringWithFormat:@"setIds('%@', '%@', '%@', '%@', '%@', '%@');",
                                                    [App getDeviceId], [App getUniqueId], @"iOS", [App iOSVerstion], [App applicationVersion], debug
                                                    ] stopProgress:YES];
                    }
                    reloading_ = NO; //リロードボタン有効化
                    force_ = NO;
                }else{
                    reloading_ = NO; //リロードボタン有効化
                    [self reloadTop:YES];
                }
            }];
        }
    }
}

- (void)reloadEnable
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{ //リロード失敗時のタイムアウト
        if(reloading_) {
            reloading_ = NO; //リロードボタン有効化
#ifdef DEBUG
            NSLog(@"___________________________________________________!");
#endif
        }
    });
}

- (void)removeInitalImage
{
    //初回起動時のみ表示されている
    if(splashView_) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{ //1秒だけ待つ 遅くなったと言われる可能性あり
            if(splashView_) {
                [splashView_ removeFromSuperview];
                splashView_ = nil;
            }
        });
    }
}

- (BOOL)checkHasApplication:(NSURL*)url {
    //    NSString *urtStr = [url absoluteString];
    NSString *host = [url host];
    //
    if([host rangeOfString:@"facebook.com"].location != NSNotFound) {//facebook
        NSString *canonical = [NSString stringWithFormat:@"%@", [url path]];
        //        NSString *escapedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
        //                                                                                                        NULL,
        //                                                                                                        (CFStringRef)canonical,
        //                                                                                                        NULL,
        //                                                                                                        CFSTR("!*'();:@&=+$,/?%#[]"),
        //                                                                                                        kCFStringEncodingUTF8));
        //        NSURL *fbURL = [NSURL URLWithString:[NSString stringWithFormat:@"fb://faceweb/f?href=%@", escapedString]];
        //        NSURL *fbURL = [NSURL URLWithString:[NSString stringWithFormat:@"fb://profile/%@", escapedString]];
        NSURL *fbURL = [NSURL URLWithString:[NSString stringWithFormat:@"fb://profile%@", canonical]];
        if([[UIApplication sharedApplication] canOpenURL:fbURL]) {
            [[UIApplication sharedApplication] openURL:fbURL];
            return YES;
        }
    }
    
    return NO;
}

- (void)checkCopyDB:(NSString*)path
{
    NSFileManager *fm = [NSFileManager defaultManager];
    if(![fm fileExistsAtPath:path]) {
        NSString *sql1 = [App getConfigSetting:@"create_data_table"];
        NSString *sql2 = [App getConfigSetting:@"create_notification_table"];
        FMDatabase *db = [FMDatabase databaseWithPath:path];
        [db open];
        [db executeUpdate:sql1];
        [db executeUpdate:sql2];
        [db close];
        [App backupOffDir:path];
    }
}

- (void)jsAccessLocalDB:(NSString*)schem cmd:(NSString*)cmd path:(NSArray*)path keyValues:(NSDictionary*)keyValues
{
    NSString* sql = [[keyValues valueForKey:@"sql"] stringByRemovingPercentEncoding];
    NSString* callbackKey = [keyValues valueForKey:@"key"];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dbPath = [NSString stringWithFormat:@"%@/%@", [paths objectAtIndex:0], [App getConfigSetting:@"dbName"]];
    [self checkCopyDB:dbPath];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    [db open];
    
#ifdef DEBUG
    NSLog(@"SQL:%@",sql);
#endif
    if ([sql hasPrefix:@"select"]) {
        NSError *error;
        FMResultSet *results = [db executeQuery:sql];
        NSMutableArray *jsonList = [[NSMutableArray alloc] initWithCapacity:0];
        while( [results next] ){
            NSMutableDictionary *json = [[NSMutableDictionary alloc] initWithCapacity:0];
            [json setValue:[NSNumber numberWithInt:[results intForColumn:@"id"]] forKey:@"id"];
            [json setValue:[results stringForColumn:@"data_kind"] forKey:@"data_kind"];
            NSData *data = [[results stringForColumn:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
            NSMutableDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            [json setValue:dataDic forKey:@"data"];
            [json setValue:[App dateToStr:[results dateForColumn:@"sort_date"]] forKey:@"sort_date"];
            [json setValue:[App dateToStr:[results dateForColumn:@"created"]] forKey:@"created"];
            [json setValue:[App dateToStr:[results dateForColumn:@"modified"]] forKey:@"modified"];
            [jsonList addObject:json];
        }
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonList options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@, '%@');", callbackKey, [jsonStr stringByReplacingOccurrencesOfString:@"\n" withString:@""]]];
    } else if ([sql hasPrefix:@"insert"]) {
        BOOL exec = [db executeUpdate:sql];
        NSString *jsonStr = nil;
        if(exec){
            [db commit];
            NSError *error;
            NSString *lastRow = @"select * from local_storage where ROWID = last_insert_rowid();";
            FMResultSet *results = [db executeQuery:lastRow];
            if( [results next] ){
                NSMutableDictionary *json = [[NSMutableDictionary alloc] initWithCapacity:0];
                [json setValue:[NSNumber numberWithInt:[results intForColumn:@"id"]] forKey:@"id"];
                [json setValue:[results stringForColumn:@"data_kind"] forKey:@"data_kind"];
                NSData *data = [[results stringForColumn:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableDictionary *dataDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                [json setValue:dataDic forKey:@"data"];
#ifdef DEBUG
                NSLog(@"%@", [results dateForColumn:@"sort_date"]);
                NSLog(@"%@", [results dateForColumn:@"created"]);
#endif
                [json setValue:[App dateToStr:[results dateForColumn:@"sort_date"]] forKey:@"sort_date"];
                [json setValue:[App dateToStr:[results dateForColumn:@"created"]] forKey:@"created"];
                [json setValue:[App dateToStr:[results dateForColumn:@"modified"]] forKey:@"modified"];
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:&error];
                jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            }
        }else{
            NSMutableDictionary *json = [[NSMutableDictionary alloc] initWithCapacity:0];
            [json setValue:[NSNumber numberWithInt:-99] forKey:@"id"];
            [json setValue:@"" forKey:@"data_kind"];
            [json setValue:@{@"message":[db lastErrorMessage]} forKey:@"data"];
            [json setValue:[NSDate date] forKey:@"sort_date"];
            [json setValue:[NSDate date] forKey:@"created"];
            [json setValue:[NSDate date] forKey:@"modified"];
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:&error];
            jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@, '%@');", callbackKey, [jsonStr stringByReplacingOccurrencesOfString:@"\n" withString:@""]]];
    } else {
        BOOL exec = [db executeUpdate:sql];
        NSString* result = @"false";
        if (exec) {
            [db commit];
            result = @"true";
        }
        [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@, '%@');", callbackKey, result]];
        
#ifdef DEBUG
        FMResultSet *results = [db executeQuery:@"select * from local_storage"];
        while( [results next] ){
            NSString* json = [results stringForColumn:@"data"];
            NSLog(@"%@",json);
        }
#endif
    }
    
    [db close];
}

- (void)jsAjaxExec:(NSDictionary*)keyValues
{
    NSString* method = [[keyValues valueForKey:@"method"] lowercaseString];
    NSString*jsonStr=[[keyValues valueForKey:@"data"] stringByRemovingPercentEncoding];
    NSString* callbackKey = [[keyValues valueForKey:@"key"] stringByRemovingPercentEncoding];
    NSURL* url = [NSURL URLWithString:[[keyValues valueForKey:@"url"] stringByRemovingPercentEncoding]];
    
    if (!url) {
        [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@, 500);", callbackKey]];
        return;
    }
    
    NSDictionary *data = @{@"":jsonStr};
//    [App displayProgressWithView:self.view];
    DownloadManager *dm = [DownloadManager buildManager];
    if ([method isEqualToString:@"get"]) {
        [dm offProgressDownload:url
                        succeed:^(NSURL *path) {
//                            [App hiddenProgress];
                            NSError *error;
                            NSData *data = [NSData dataWithContentsOfURL:path options:NSDataReadingUncached error:&error];
                            NSString *jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                            if (!error) {
                                jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                                jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"'" withString:@"\\'"];
                                [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@, '{\"result\":200, \"data\":%@}');", callbackKey, jsonStr]];
                            } else {
                                /*
                                 view.evaluateJavascript(String.format("javascript:pop(%s, '{\"result\":500}');", callbackKey), null);
                                 */
                                [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@, '{\"result\":%ld}');", callbackKey,error.code]];
                            }
                        }
                         failed:^(NSError *error, NSURL *path) {
//                             [App hiddenProgress];
                             [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@, '{\"result\":%ld}');", callbackKey,error.code]];
                         }];
    } else if ([method isEqualToString:@"post"]
               || [method isEqualToString:@"put"]
               || [method isEqualToString:@"delete"]) {
        [dm offProgressDownload:url
                          datas:data
                        succeed:^(NSURL *path) {
//                            [App hiddenProgress];
                            NSError *error;
                            NSData *data = [NSData dataWithContentsOfURL:path options:NSDataReadingUncached error:&error];
                            NSString *jsonStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                            if (!error) {
                                jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                                jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"'" withString:@"\\'"];
                                [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@, '{\"result\":200, \"data\":%@}');", callbackKey, jsonStr]];
                            }else{
                                [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@, '{\"result\":%ld}');", callbackKey,error.code]];
                            }
                        }
                         failed:^(NSError *error, NSURL *path) {
//                             [App hiddenProgress];
                             [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@, '{\"result\":%ld}');", callbackKey,error.code]];
                         }
                         method:[method uppercaseString]];
    }
}

#pragma mark WKUIDelegate

- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration
   forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures
{
    return webView;
}

- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message
initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler
{
    
}

- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message
initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL result))completionHandler
{
    
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText
initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString *result))completionHandler
{
    
}

- (void)webViewDidClose:(WKWebView *)webView
{
    
}

#pragma mark WKNavigationDelegate

- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation
{
#ifdef DEBUG
    NSLog(@"didCommitNavigation !!");
#endif
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error //didFailLoadWithError
{
#ifdef DEBUG
    NSLog(@"didFailNavigation !!");
#endif
    reloading_ = NO; //リロードボタン有効化
    [self reloadTop:YES];
    [self removeInitalImage];
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
#ifdef DEBUG
    NSLog(@"didFailProvisionalNavigation !!!!!!!!!");
#endif
    reloading_ = NO; //リロードボタン有効化
    [self reloadTop:YES];
    [self removeInitalImage];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation //webViewDidFinishLoad
{
#ifdef DEBUG
    NSLog(@"didFinishNavigation !!");
#endif
    [self finishWebLoad:webView];
}

- (void)finishWebLoad:(WKWebView *)webView
{
    [webView evaluateJavaScript:@"document.documentElement.style.webkitUserSelect='none';" completionHandler:^(id obj, NSError *error) {
    }];
    [webView evaluateJavaScript:@"document.documentElement.style.webkitTouchCallout='none';" completionHandler:^(id obj, NSError *error) {
    }];
    
    NSString *bundleVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleVersion"];
    NSString *bundleVersionName = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    [webView evaluateJavaScript:[NSString stringWithFormat:@"javascript:var androidVersionCode='%@';", bundleVersion] completionHandler:^(id obj, NSError *error) {
    }];
    [webView evaluateJavaScript:[NSString stringWithFormat:@"javascript:var androidVersionName='%@';", bundleVersionName] completionHandler:^(id obj, NSError *error) {
    }];
    
    if(reload_) {
        reload_ = NO;
        if(![App isSleepingOrBackground]) {
            //DEBUG
            //DEBUG
            //            [webView evaluateJavaScript:[NSString stringWithFormat:@"setIds('%@', '%@');", [App getDeviceId], [App getUniqueId]] completionHandler:^(id obj, NSError *error) {
            //            }];
            //DEBUG
            //DEBUG
            NSString *debug = @"false";
#ifdef DEBUG
            debug = @"true";
#endif
            [self evaluatingJavaScript:[NSString stringWithFormat:@"setIds('%@', '%@', '%@', '%@', '%@', '%@');",
                                        [App getDeviceId], [App getUniqueId], @"iOS", [App iOSVerstion], [App applicationVersion], debug
                                        ] stopProgress:YES];
#ifdef DEBUG
            NSLog(@"===================================!");
#endif
        }
    }
    reloading_ = NO; //リロードボタン有効化
    [self removeInitalImage];
}

- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
{
#ifdef DEBUG
    NSLog(@"didReceiveAuthenticationChallenge !!");
#endif
    completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
}

- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation
{
    
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation //webViewDidStartLoad
{
    
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction //shouldStartLoadWithRequest
decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    //キャッシュクリア
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    NSURL *url = [navigationAction.request URL];
    NSString *scheme = [url scheme];
    NSString *cmd = [[url host] lowercaseString];
    // host:コマンド
    // [[url pathComponents] objectAtIndex:1]:サブコマンド
    // [[url pathComponents] objectAtIndex:2]:ストレージKey
    // [[url pathComponents] objectAtIndex:3]:ストレージValue
    // lastPathComponent:JSコールバック用ハッシュ
    // query:URL、Cache URL、
    
    if ([scheme isEqualToString:@"setting"]) {
        //設定画面表示
//        UINavigationController *settingNavi = [getSettingStoryBoard instantiateViewControllerWithIdentifier:@"SettingNavi"];
//        [self presentViewController:settingNavi animated:YES completion:^{
//        }];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"eula"]) {
        //利用規約表示
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"privacy"]) {
        //プライバシーポリシー表示
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"bottominbrowser"] || [scheme isEqualToString:@"bottominbrowsers"]) {
        //下からのトランジションで別WebView
        NSString *path = [url.absoluteString componentsSeparatedByString:@"localhost"][1];
        NSString *requestUrl = [NSString stringWithFormat:@"http://localhost:%ld%@", (long)[self getPort], path];
        UIStoryboard* storyBoard = getStoryBoard;
        WKCMSViewController *tergetViewCtrl = [storyBoard instantiateViewControllerWithIdentifier:@"WKCMSViewController"];
        tergetViewCtrl.noSplash_ = YES;
        [tergetViewCtrl loadRequest:[NSURL URLWithString:requestUrl]];
        [self presentViewController:tergetViewCtrl
                           animated:YES
                         completion:^{
                         }
         ];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"rightinbrowser"] || [scheme isEqualToString:@"rightinbrowsers"]) {
        //右からのトランジションで別WebView
        NSString *path = [url.absoluteString componentsSeparatedByString:@"localhost"][1];
        NSString *requestUrl = [NSString stringWithFormat:@"http://localhost:%ld%@", (long)[self getPort],path];
        UIStoryboard* storyBoard = getStoryBoard;
        WKCMSViewController *tergetViewCtrl = [storyBoard instantiateViewControllerWithIdentifier:@"WKCMSViewController"];
        tergetViewCtrl.noSplash_ = YES;
        [tergetViewCtrl loadRequest:[NSURL URLWithString:requestUrl]];
        [self.navigationController pushViewController:tergetViewCtrl animated:YES];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"dissmiss"]) {
        [self.navigationController popViewControllerAnimated:YES];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"pop"]) {
        [self dismissViewControllerAnimated:YES completion:^{}];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"ttp"] || [scheme isEqualToString:@"ttps"]) {
        //外部ブラウザで表示
        NSString *urlStr = [NSString stringWithFormat:@"h%@", url.absoluteString];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"notification"]) {
        //ローカル通知設定
        NSDictionary *keyValues = [self parseQuery:[url query]];
        NSString *title = [[keyValues valueForKey:@"title"] stringByRemovingPercentEncoding];
        NSString *text = [[keyValues valueForKey:@"text"] stringByRemovingPercentEncoding];
        NSString *subtext = [[keyValues valueForKey:@"subtext"] stringByRemovingPercentEncoding];
        NSString *stId = [[keyValues valueForKey:@"stId"] stringByRemovingPercentEncoding];
        NSString *date = [[keyValues valueForKey:@"date"] stringByRemovingPercentEncoding];
        NSString* callbackKey = [keyValues valueForKey:@"key"];
        NSString *control = [[url pathComponents][1] stringByRemovingPercentEncoding];
        
        if ([control isEqualToString:@"add"]) {
            UILocalNotification *localNotification = [UILocalNotification new];
            localNotification.alertBody = text;
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            localNotification.userInfo = @{@"title":title,
                                           @"text":text,
                                           @"subtext":subtext,
                                           @"stId":stId,
                                           @"date":date,
                                           @"key":callbackKey};
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"]];
            [df setDateFormat:@"yyyyMMddHHmm"];
            NSDate *fireDate = [df dateFromString:date];
            
            if (!fireDate) {
                [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@, '{\"error\":\"%@\"}');", callbackKey, @""]];
                return;
            }
            
            [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@, '{\"id\":\"%@\"}');", callbackKey, stId]];
            
            localNotification.fireDate = fireDate;
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        } else if ([control isEqualToString:@"mod"]) {
            NSString *targetId = [[keyValues valueForKey:@"id"] stringByRemovingPercentEncoding];
            for(UILocalNotification *notification in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
                if ([notification.userInfo[@"stId"] isEqualToString:targetId]) {
                    UILocalNotification *localNotification = [UILocalNotification new];
                    localNotification.alertBody = text;
                    //            localNotification.category = category;
                    localNotification.soundName = UILocalNotificationDefaultSoundName;
                    localNotification.userInfo = @{@"title":title,
                                                   @"text":text,
                                                   @"subtext":subtext,
                                                   @"stId":stId,
                                                   @"date":date,
                                                   @"key":callbackKey};
                    NSDateFormatter *df = [[NSDateFormatter alloc] init];
                    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"]];
                    [df setDateFormat:@"yyyyMMddHHmm"];
                    NSDate *fireDate = [df dateFromString:date];
                    
                    if (!fireDate) {
                        [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@, '{\"error\":\"%@\"}');", callbackKey, @""]];
                        return;
                    }
                    
                    [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@, '{\"id\":\"%@\"}');", callbackKey, targetId]];
#ifdef DEBUG
                    NSLog(@"OLD:%@", [notification.userInfo valueForKey:@"date"]);
                    NSLog(@"NEW:%@", date);
#endif
                    localNotification.fireDate = fireDate;
                    [[UIApplication sharedApplication] cancelLocalNotification:notification];
                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                    break;
                }
            }
        } else if ([control isEqualToString:@"del"]) {
            NSString *targetId = [[keyValues valueForKey:@"id"] stringByRemovingPercentEncoding];
            for(UILocalNotification *notification in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
                if ([notification.userInfo[@"stId"] isEqualToString:targetId]) {
                    [[UIApplication sharedApplication] cancelLocalNotification:notification];
                    break;
                }
            }
        }
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"gatrack"]) {
        //GAトラッキング
        NSDictionary *keyValues = [self parseQuery:[url query]];
        NSString *title = [[keyValues valueForKey:@"title"] stringByRemovingPercentEncoding];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:title];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        [tracker set:kGAIScreenName value:title];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"gaevent"]) {
        //GAイベント
        NSDictionary *keyValues = [self parseQuery:[url query]];
        NSString *category = [[keyValues valueForKey:@"category"] stringByRemovingPercentEncoding];
        NSString *action = [[keyValues valueForKey:@"action"] stringByRemovingPercentEncoding];
        NSString *label = [[keyValues valueForKey:@"label"] stringByRemovingPercentEncoding];
        NSString *screen = [[keyValues valueForKey:@"screen"] stringByRemovingPercentEncoding];
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:screen];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category action:action label:label value:nil] build]];
        [tracker set:kGAIScreenName value:screen];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"dbaccess"]) { //今週中
        //DB操作
        //dbaccess://localhost/add?sql="select * form "
        NSArray* path = [url.path pathComponents];
        NSDictionary *keyValues = [self parseQuery:[url query]];
        [self jsAccessLocalDB:scheme cmd:cmd path:path keyValues:keyValues];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"ajax"]) { //今週中
        //通信
        NSDictionary *keyValues = [self parseQuery:[url query]];
        [self jsAjaxExec:keyValues];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"alert"]) { //今週中
        NSDictionary *keyValues = [self parseQuery:[url query]];
        NSString *title = [[keyValues valueForKey:@"title"] stringByRemovingPercentEncoding];
        NSString *message = [[keyValues valueForKey:@"message"] stringByRemovingPercentEncoding];
        NSString* callbackKey = [keyValues valueForKey:@"key"];
        NSString *buttonName = [[keyValues valueForKey:@"buttonName"] stringByRemovingPercentEncoding] == nil ? @"閉じる":[[keyValues valueForKey:@"buttonName"] stringByRemovingPercentEncoding] ;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:buttonName style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@);",callbackKey]];
            
        }]];
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"confirm"]) { //今週中
        //ネイティブ
        NSDictionary *keyValues = [self parseQuery:[url query]];
        NSString *title = [[keyValues valueForKey:@"title"] stringByRemovingPercentEncoding];
        NSString *message = [[keyValues valueForKey:@"message"] stringByRemovingPercentEncoding];
        NSString* callbackKey = [keyValues valueForKey:@"key"];
        NSString *buttonName = [[keyValues valueForKey:@"buttonName"] stringByRemovingPercentEncoding] == nil ? @"OK":[[keyValues valueForKey:@"buttonName"] stringByRemovingPercentEncoding] ;
        NSString *buttonCancel = [[keyValues valueForKey:@"buttonCancel"] stringByRemovingPercentEncoding] == nil ? @"キャンセル":[[keyValues valueForKey:@"buttonCancel"] stringByRemovingPercentEncoding] ;
        
        UIAlertControllerStyle style = UIAlertControllerStyleAlert;
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:style];
        if ([keyValues valueForKey:@"others"] && [[[keyValues valueForKey:@"others"] description] length] > 0) {
            NSArray *others = [[keyValues valueForKey:@"others"] componentsSeparatedByString:@","];
            if ([others count] > 0) {
                for (int i = 0; [others count] > i; ++ i) {
                    NSString* other = [others[i] stringByRemovingPercentEncoding];
                    if ([other length] == 0)
                        continue;
                    
                    [alert addAction:[UIAlertAction actionWithTitle:other style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@,%@);",callbackKey,other]];
                    }]];
                }
            } else if ([keyValues valueForKey:@"others"]) {
                NSString* other = [[keyValues valueForKey:@"others"] stringByRemovingPercentEncoding];
                [alert addAction:[UIAlertAction actionWithTitle:other style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@,%@);",callbackKey,other]];
                }]];
            }
        }
        
        [alert addAction:[UIAlertAction actionWithTitle:buttonName style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@,'OK');",callbackKey]];
            
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:buttonCancel style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@,'cancel');",callbackKey]];
        }]];
        
        [self presentViewController:alert animated:YES completion:^{
            NSLog(@"confirm visible");
        }];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"gettoken"]) {
//        NSDictionary *keyValues = [self parseQuery:[url query]];
//        NSString *accessToken = [App getAccessToken];
//        NSString* callbackKey = [keyValues valueForKey:@"key"];
//        [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@, '{\"token\":\"%@\"}');", callbackKey, accessToken]];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"debugnotitificaitonon"]) {
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"debugnotitificaitonff"]) {
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"netcheck"]) {
        BOOL isConnect = YES;
        NSDictionary *keyValues = [self parseQuery:[url query]];
        NSString* callbackKey = [keyValues valueForKey:@"key"];
        
        DownloadManager *dm = [DownloadManager buildManager];
        NETWORK_STATUS nstatus = [dm checkNetwork];
        if(nstatus != CONNECT && nstatus != CONNECT_3G)
            isConnect = NO;
        [self evaluatingJavaScript:[NSString stringWithFormat:@"javascript:pop(%@, '%d');", callbackKey, isConnect]];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([scheme isEqualToString:@"log"]) { //今週中
        //開発ログ
        decisionHandler(WKNavigationActionPolicyCancel);
    } else {
        if([App isExportDomains:url]) {
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
                decisionHandler(WKNavigationActionPolicyCancel);
            }
        }
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse
decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler
{
    decisionHandler(WKNavigationResponsePolicyAllow);
}

@end
