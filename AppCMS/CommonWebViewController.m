//
//  CommonWebViewController.m
//  AppCMS
//
//  Created by 長島 伸光 on 2015/03/19.
//  Copyright (c) 2015年 MEAS. All rights reserved.
//

#import "CMSAppDelegate.h"
#import "DownloadManager.h"
#import "WKCMSViewController.h"
#import "CommonWebViewController.h"

@interface CommonWebViewController () {
    BOOL isSetting_;
}

@end

@implementation CommonWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    config.allowsInlineMediaPlayback = YES;
    if(![App isUnderIOS9]) {
        config.allowsPictureInPictureMediaPlayback = YES;
        config.allowsAirPlayForMediaPlayback = YES;
    }
    webView_ = [[WKWebView alloc] initWithFrame:self.view.frame configuration:config];
    [self.view addSubview:webView_];
    //AutoLayout
    [self setAutoLayout];

    webView_.UIDelegate = self;
    webView_.navigationDelegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setAutoLayout
{
    [webView_ setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(webView_);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[webView_]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:viewsDictionary]
     ];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-64-[webView_]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:viewsDictionary]
     ];
    [self.view layoutIfNeeded];
    
}

- (IBAction)back:(id)sender
{
    if(!isSetting_) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        });
        return;
    }
    //ユーザ設定を保存
    NSError *error;
    NSURL *settingURL = [NSURL URLWithString:[App getConfigSetting:@"settingInfoURL"]];
    NSMutableDictionary *json = [[NSMutableDictionary alloc] initWithCapacity:0];
    NSString *strDeviceToken = [App getDeviceId];
    NSString *UUIDString = [App getUniqueId];
    NSDictionary *data = @{@"device_id":strDeviceToken, @"unique_id":UUIDString, @"ver":[App getAPIVersion]};
    [json setValue:data forKey:@"regist"];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *params = @{@"":jsonStr};

    WKCMSViewController *vc = [App getRootWebViewController];
    DownloadManager *dm = [DownloadManager buildManager];
    [dm offProgressDownloadWithCache:settingURL
                               datas:params
//                               cache:[NSURL URLWithString:[NSString stringWithFormat:@"http://127.0.0.1:%ld/setting.json", (long)[vc getPort]]]
                               cache:[NSURL URLWithString:[NSString stringWithFormat:@"http://localhost:%ld/setting.json", (long)[vc getPort]]]
                             succeed:^(NSURL *path) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        });
    } failed:^(NSError *error, NSURL *path) {
        NSLog(@"%@", [error localizedDescription]);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        });
    }];
}

- (void)setURL:(NSString*)url isSetting:(BOOL)isSetting title:(NSString*)title
{
    isSetting_ = isSetting;
    titleItem_.title = title;
    [webView_ loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
//    webView_.allowsInlineMediaPlayback = YES;
}

- (void)setLocalURL:(NSString*)url isSetting:(BOOL)isSetting title:(NSString*)title
{
    isSetting_ = isSetting;
    titleItem_.title = title;
    WKCMSViewController *vc = [App getRootWebViewController];
    [webView_ loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:url, [vc getPort]]]]];
//    webView_.allowsInlineMediaPlayback = YES;
}

#pragma mark WKUIDelegate

- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration
        forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures
{
    return webView;
}

- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message
        initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler
{
    
}

- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message
        initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL result))completionHandler
{
    
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText
        initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString *result))completionHandler
{
    
}

- (void)webViewDidClose:(WKWebView *)webView
{
    
}

#pragma mark WKNavigationDelegate

- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation
{
#ifdef DEBUG
    NSLog(@"didCommitNavigation !!");
#endif
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error //didFailLoadWithError
{
#ifdef DEBUG
    NSLog(@"%@", error.localizedDescription);
#endif
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
#ifdef DEBUG
    NSLog(@"didFailProvisionalNavigation !!");
#endif
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation //webViewDidFinishLoad
{
#ifdef DEBUG
    NSLog(@"didFinishNavigation !!");
#endif
    [webView evaluateJavaScript:@"document.documentElement.style.webkitUserSelect='none';" completionHandler:^(id obj, NSError *error) {
    }];
    [webView evaluateJavaScript:@"document.documentElement.style.webkitTouchCallout='none';" completionHandler:^(id obj, NSError *error) {
    }];
    [webView evaluateJavaScript:@"document.title;" completionHandler:^(id obj, NSError *error) {
        if(obj && [obj isKindOfClass:[NSString class]]) {
            titleItem_.title = (NSString*)obj;
        }
    }];
}

- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
        completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
{
    completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
}

- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation
{
    
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation //webViewDidStartLoad
{
    
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction //shouldStartLoadWithRequest
decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    //キャッシュクリア
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
//    NSURL *url = [navigationAction.request URL];
//    NSString *scheme = [url scheme];
//    NSString *cmd = [[url host] lowercaseString];
    // host:コマンド
    // [[url pathComponents] objectAtIndex:1]:サブコマンド
    // [[url pathComponents] objectAtIndex:2]:ストレージKey
    // [[url pathComponents] objectAtIndex:3]:ストレージValue
    // lastPathComponent:JSコールバック用ハッシュ
    // query:URL、Cache URL、
//    if ([scheme isEqualToString:@"appcms"]) {
//        decisionHandler(WKNavigationActionPolicyCancel);
//    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse
        decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler
{
    decisionHandler(WKNavigationResponsePolicyAllow);
}

@end
