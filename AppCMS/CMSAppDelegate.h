//
//  AppDelegate.h
//  AppCMS
//
//  Created by 長島 伸光 on 2014/12/31.
//  Copyright (c) 2014年 MEAS. All rights reserved.
//

#define App (CMSAppDelegate *)[[UIApplication sharedApplication] delegate]
#define getStoryBoard [UIStoryboard storyboardWithName:@"CMSMain" bundle:nil]

#define FORGRAND_END_NOTIFICATION @"ForgrandEndhNotification@@"
#define SPLASH_START_NOTIFICATION @"SplashStarthNotification@@"
#define SPLASH_END_NOTIFICATION @"SplashEndhNotification@@"

#define DOWNLOAD_START_NOTIFICATION  @"DownloadStartNotification@@"
#define DOWNLOAD_UPDATE_NOTIFICATION @"DownloadUpdateNotification@@"
#define DOWNLOAD_END_NOTIFICATION    @"DownloadEndNotification@@"

#define LATEST_LAUNCH_VERSION @"latestLaunchVersion"

#import <UIKit/UIKit.h>
@class WKCMSViewController;

@protocol NotificationBroadcastDelegate

@optional
- (void) notificatoinRemote:(NSDictionary*)userInfo;
- (void) notificatoinLocal:(NSDictionary*)userInfo;
- (void) notificationSettingComp:(NSDictionary*)userInfo;

@end

@interface CMSAppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate> {
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIView *progressView;
@property (readwrite) BOOL updateUI;
@property (readwrite) BOOL isSleepingOrBackground;
@property (nonatomic, strong) NSString *launchURL;

- (NSString*)iOSVerstion;
- (NSString*)applicationVersion;
- (BOOL)isUnderIOS9;
- (BOOL)isiPhone;
- (NSUUID*)uuid;
- (NSString*)documentRoot;
- (NSString*)cacheRoot;
- (void)setSetting:(NSString*)key value:(NSObject*)value;
- (NSObject*)getSetting:(NSString*)key;
- (NSString*)getDeviceId;
- (NSString*)getUniqueId;
- (NSString*) getAPIVersion;
- (void)removeSetting:(NSString*)key;
- (NSString*) getConfigSetting:(NSString*)key;
- (void)displayProgress;
- (void)displayProgressWithView:(UIView*)view;
- (void)hiddenProgress;
- (BOOL)isExportDomains:(NSURL*)url;
- (void)setNotificationDelegate:(id<NotificationBroadcastDelegate>)notification;
- (NSString*)getLocalizedString:(NSString*)key;
- (NSString*)getVersion;
- (NSString*)getVersionName;
- (WKCMSViewController*)getRootWebViewController;


//- (void)alert:(NSString*)message;
//- (void)alert:(NSString*)message code:(NSInteger)code;
//- (void)alert:(NSString*)message code:(NSInteger)code callback:(void(^)())callback;
//- (void)warning:(NSString*)message;

//backup対象から外す
- (void)backupOffDir:(NSString*)path;
- (void)setSkipBackupAttributeToItemAtPath:(NSString *) filePathString;
- (NSString*)dateToStr:(NSDate*)date;

@end

