//
//  NewsContent.h
//  AppCMS
//
//  Created by 長島 伸光 on 2015/03/13.
//  Copyright (c) 2015年 MEAS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsContent : NSObject {
}

@property (nonatomic, retain) NSString *date;
@property (nonatomic, retain) NSDate *issue_date;
@property (nonatomic, retain) NSDate *update_date;
@property (nonatomic, retain) NSDate *release_date;
@property (nonatomic, retain) NSDate *close_date;
@property (nonatomic, retain) NSString *news_id;
@property (nonatomic, retain) NSString *news_title;
@property (nonatomic, retain) NSString *news_detail;
@property (nonatomic, retain) NSString *news_genre_id;
@property (nonatomic, retain) NSString *news_genre_tag;
@property (nonatomic, readwrite) BOOL pickup_flg;
@property (nonatomic, retain) NSString *thumb_path;
@property (nonatomic, retain) NSString *detail_json_path;
@property (nonatomic, retain) NSString *web_url;
@property (nonatomic, retain) NSString *movie_url;

@end
