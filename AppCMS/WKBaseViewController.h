//
//  WKBaseViewController.h
//  AppCMS
//
//  Created by 長島 伸光 on 2016/12/30.
//  Copyright © 2016年 MEAS. All rights reserved.
//

#import <WebKit/WebKit.h>
#import <UIKit/UIKit.h>
#import <CocoaHTTPServer/HTTPServer.h>
#import <GAI.h>
#import <GAIFields.h>
#import <GAIDictionaryBuilder.h>

#define rootUrlString(port) [NSString stringWithFormat:@"http://localhost:%ld/index.html", (long)port]

@interface WKBaseViewController : UIViewController <WKNavigationDelegate, WKUIDelegate> {
    WKWebView *webView_;
    BOOL reloading_;
    BOOL force_;
    HTTPServer *httpServer_;
    UIImageView *splashView_;
}

@property (nonatomic, readwrite) BOOL reload_;
@property (nonatomic, readwrite) BOOL noSplash_;
@property (nonatomic, readonly, nullable) WKWebView *webView_;

- (void) evaluatingJavaScript:(NSString* __nullable)script;
- (void) evaluatingJavaScript:(NSString* __nullable)script stopProgress:(BOOL)stopProgress;
- (void)hasDocument:(void(^ __nullable)(BOOL result)) callback;
- (void)endSetId;
- (void)loadRequest:(NSURL* __nullable)url;
- (void)reloadTop:(BOOL)reload;
- (void)reloadEnable;
- (void)removeInitalImage;
- (void)reloadView;
- (NSDictionary* __nullable)parseQuery:(NSString* __nullable)query;

- (NSInteger)getPort;

@end
