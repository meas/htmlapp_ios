//
//  NewsContent.m
//  AppCMS
//
//  Created by 長島 伸光 on 2015/03/13.
//  Copyright (c) 2015年 MEAS. All rights reserved.
//

#import "NewsContent.h"

@implementation NewsContent

@synthesize date;
@synthesize issue_date;
@synthesize update_date;
@synthesize release_date;
@synthesize close_date;
@synthesize news_id;
@synthesize news_title;
@synthesize news_detail;
@synthesize news_genre_id;
@synthesize news_genre_tag;
@synthesize pickup_flg;
@synthesize thumb_path;
@synthesize detail_json_path;
@synthesize web_url;
@synthesize movie_url;

@end
