//
//  AppDelegate.m
//  AppCMS
//
//  Created by 長島 伸光 on 2014/12/31.
//  Copyright (c) 2014年 MEAS. All rights reserved.
//

#import "CMSAppDelegate.h"
#import "DownloadManager.h"
#import "WKCMSViewController.h"
#import <GAI.h>
#import <notify.h>
#import <AudioToolbox/AudioServices.h>
#import <SSZipArchive/SSZipArchive.h>

@interface CMSAppDelegate () {
    id<NotificationBroadcastDelegate> notificattion_;
    BOOL pregoress_;
}

@end

@implementation CMSAppDelegate

@synthesize launchURL;

//- (void)alertUpdateUI
//{
//    UIAlertController *alert = nil;
//    alert = [UIAlertController alertControllerWithTitle:@""
//                                                message:@"コンテンツを更新します。"
//                                         preferredStyle:UIAlertControllerStyleAlert];
//    [alert addAction:[UIAlertAction actionWithTitle:@"OK"
//                                              style:UIAlertActionStyleDefault
//                                            handler:^(UIAlertAction *action) {
//                                                [self changeUI];
//                                            }]];
//    [[self.window rootViewController] presentViewController:alert animated:YES completion:^{
//    }];
//}

- (void)changeUI
{
    //プログレス表示
    [App displayProgress];
    WKCMSViewController *vc = [self getRootWebViewController];
    if ([vc respondsToSelector:@selector(startHttpServer:)]) {
        [vc stopHttpServer];
        //httpサーバ終了待ち
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [vc startHttpServerAndReload:0];
            [self hiddenProgress];
        });
    }else{
        [self hiddenProgress];
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    //初期設定
    _isSleepingOrBackground = NO;
    pregoress_ = NO;
    application.statusBarStyle = UIStatusBarStyleLightContent;
    
    //プログレス表示
    [App displayProgress];

    //リモート通知セットアップ
    UIUserNotificationType types = UIUserNotificationTypeSound|UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [application registerUserNotificationSettings:mySettings];

    self.progressView = [[UIView alloc] initWithFrame:self.window.frame];
    self.progressView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    self.progressView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.96];
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [indicator startAnimating];
    [self.progressView addSubview:indicator];
    indicator.center = self.window.center;
    application.applicationIconBadgeNumber = -1;

    //
    [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    //DocumenntRoot確認
    if(![self hasDocumentRoot] || [self checkUpdate]) {
        NSError *error;
        NSFileManager *fm = [NSFileManager defaultManager];
        NSString *uiFile = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"htdocs/ui.zip"];
        [fm removeItemAtPath:[self documentRoot] error:&error];
        if([SSZipArchive unzipFileAtPath:uiFile toDestination:[self documentRoot]]) {
        }
        //ドキュメントルートをバックアップ対象外に
        [self backupOffDir:[self documentRoot]];
        [self setSetting:LATEST_LAUNCH_VERSION value:[self getVersionName]];
    }
    self.updateUI = NO;
    //Download処理後必ず起動する
    WKCMSViewController *vc = [self getRootWebViewController];
    vc.reload_ = YES;
    vc.noSplash_ = NO;
    [vc startHttpServer:0 reRequest:NO isLaunch:YES];
    
    [[GAI sharedInstance] trackerWithTrackingId:[self getConfigSetting:@"googleAnalytics"]];
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    //ローカル通知
    NSDictionary* userInfoLocal = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (userInfoLocal != nil && notificattion_ != nil) {
        [notificattion_ notificatoinLocal:userInfoLocal];
    }
    
    //リモート通知
    NSDictionary *userInfoRemote = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (userInfoRemote != nil && notificattion_ != nil) {
        [notificattion_ notificatoinRemote:userInfoRemote];
    }
    
    ////
    //未実装
    ////
    launchURL = nil;
//    if ((launchOptions != nil) && ([launchOptions valueForKey:UIApplicationLaunchOptionsURLKey] != nil)) {
//        NSURL *url = (NSURL *)[launchOptions valueForKey:UIApplicationLaunchOptionsURLKey];
//        launchURL = url.absoluteString;
//    }
//    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    //URLを解析してパラメータを取得
    //未実装
//    WKCMSViewController *vc = (WKCMSViewController*)[self.window rootViewController];
//    [vc evaluatingJavaScript:[NSString stringWithFormat:@"launchOther('%@');", url.absoluteString] stopProgress:YES];
    return YES;
}

-(void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];

    if (application.applicationState == UIApplicationStateActive) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [notificattion_ notificationSettingComp:nil];
        });
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    WKCMSViewController *vc = [self getRootWebViewController];
    [vc.webView_ stopLoading];
    
    _isSleepingOrBackground = YES;
    DownloadManager *dm = [DownloadManager buildManager];
    if(dm.initilaLock_) {
        [self setSetting:@"DownloadManagerLastAccess" value:[NSDate date]];
    }
}

//
//
//フォアグラウンド処理
//
//
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    id observer =
    [[NSNotificationCenter defaultCenter] addObserverForName:FORGRAND_END_NOTIFICATION object:nil queue:nil usingBlock:^(NSNotification *note) {
        [[NSNotificationCenter defaultCenter] removeObserver:observer];
        [App hiddenProgress];
    }];
    _isSleepingOrBackground = NO;
    DownloadManager *dm = [DownloadManager buildManager];
    NSDate *lastDate = (NSDate*)[self getSetting:@"DownloadManagerLastAccess"];
    if(dm.initilaLock_) {
        if(!lastDate) {
            dm.initilaLock_ = NO;
        }
    }else{
        [self removeSetting:@"DownloadManagerLastAccess"];
    }
    
    WKCMSViewController *vc = [self getRootWebViewController];
    vc.reload_ = YES;
    vc.noSplash_ = NO;
    [vc startHttpServer:0 reRequest:NO isLaunch:YES];

    application.applicationIconBadgeNumber = -1;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    WKCMSViewController *vc = [self getRootWebViewController];
    if ([vc respondsToSelector:@selector(stopHttpServer)]) {
        [vc stopHttpServer];
    }
}

- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    //バックグラウンド処理なし
    completionHandler(UIBackgroundFetchResultNewData);
    //
    //バックグラウンド処理
//    DownloadManager *dm = [DownloadManager buildManager];
//    [dm offProgressDownloadUIPackage:^(BOOL sccess) {
//        self.updateUI = sccess;
//        //バックグラウンド処理 記事処理
//        dispatch_async(dispatch_get_main_queue(), ^{ //
//            [self downloadNewsContents:completionHandler hasNewUI:NO onProgress:NO];
//        });
//    }];
}

- (void)backgroudProcess:(void (^)(UIBackgroundFetchResult))completionHandler
{
    DownloadManager *dm = [DownloadManager buildManager];
    [dm downloadBackground:^(BOOL sccess) {
        NSLog(@"-----------------  --------------------");
        if(completionHandler) {
            completionHandler(UIBackgroundFetchResultNewData);
        }
    }];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    //デバイス登録
    NSError *error;
    NSURL *url = [NSURL URLWithString:[self getConfigSetting:@"deviceRegistURL"]];
    NSMutableDictionary *json = [[NSMutableDictionary alloc] initWithCapacity:0];
    NSUUID *vendorUUID = [UIDevice currentDevice].identifierForVendor;
    NSString *strDeviceToken = [deviceToken description];
    strDeviceToken = [strDeviceToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    strDeviceToken = [strDeviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *UUIDString = vendorUUID.UUIDString;
    NSDictionary *data = @{@"device_id":strDeviceToken, @"unique_id":UUIDString, @"ver":[self getAPIVersion], @"device":@"iOS"};
    [json setValue:data forKey:@"regist"];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *params = @{@"":jsonStr};
    
    NSString *d = [self getDeviceId];
    NSString *u = [self getUniqueId];
    if(!d || ![d isEqualToString:strDeviceToken] || !u || ![u isEqualToString:vendorUUID.UUIDString]) {
        [self setDeviceId:strDeviceToken];
        [self setUniqueId:vendorUUID.UUIDString];
        DownloadManager *dm = [DownloadManager buildManager];
        [dm offProgressDownloadWithCache:url datas:params cache:nil succeed:^(NSURL *path) {
            //リモート通知登録2
            NSLog(@"device regist ");
        } failed:^(NSError *error, NSURL *path) {
            NSLog(@"%@", [error localizedDescription]);
        }];
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError: %@", error);
}

- (void)getUserSetting:(NSString*)strDeviceToken uid:(NSString*)UUIDString
{
    //ユーザ設定取得
    NSError *error;
    NSURL *settingURL = [NSURL URLWithString:[App getConfigSetting:@"settingInfoURL"]];
    NSMutableDictionary *json = [[NSMutableDictionary alloc] initWithCapacity:0];
    NSDictionary *data = @{@"device_id":strDeviceToken, @"unique_id":UUIDString, @"ver":[App getAPIVersion]};
    [json setValue:data forKey:@"regist"];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *params = @{@"":jsonStr};
    
    DownloadManager *dm = [DownloadManager buildManager];
    WKCMSViewController *vc = [self getRootWebViewController];
    [dm offProgressDownloadWithCache:settingURL
                               datas:params
//                               cache:[NSURL URLWithString:[NSString stringWithFormat:@"http://127.0.0.1:%ld/setting.json", (long)[vc getPort]]]
                               cache:[NSURL URLWithString:[NSString stringWithFormat:@"http://localhost:%ld/setting.json", (long)[vc getPort]]]
                             succeed:^(NSURL *path) {
                                 //キャッシュフォルダをバックアップ対象外に
                                 [self backupOffDir:[self cacheRoot]];
                                 [self updateNotification];
                             } failed:^(NSError *error, NSURL *path) {
                                 NSLog(@"< %@ >", [error localizedDescription]);
                             }];
}

- (void)updateNotification
{
    //設定
    NSError *error;
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *settingRealPath = [NSString stringWithFormat:@"%@/setting.json", [self cacheRoot]];
    if([fm fileExistsAtPath:settingRealPath]) {
        NSData *jsonData = [NSData dataWithContentsOfFile:settingRealPath];
        NSMutableDictionary *settingJson = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        if(error) {
            NSLog(@"SETTING JSON READ ERROR:%@", error.localizedDescription);
        }else{
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
//            NSDictionary *result = [settingJson valueForKey:@"result"];
//            NSString *morning = [result valueForKey:@"notification_morning"];
//            NSDate *now = [NSDate date];
//            NSCalendar *calendar = [NSCalendar currentCalendar];
//            NSUInteger flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
//            NSDateComponents *comps = [calendar components:flags fromDate:now];
//            if(morning && [morning intValue] >= 0) {
//                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
//                NSDateComponents *newConp = nil;
//                if(comps.hour >= [morning intValue]) {
//                    NSDate *nextDate = [NSDate dateWithTimeIntervalSinceNow:1*24*60*60];
//                    newConp = [calendar components:flags fromDate:nextDate];
//                    newConp.hour = [morning intValue];
//                    newConp.minute = 0;
//                }else{
//                    newConp = [calendar components:flags fromDate:now];
//                    newConp.hour = [morning intValue];
//                    newConp.minute = 0;
//                }
//                NSDate *fireDate = [calendar dateFromComponents:newConp];
//                localNotification.fireDate = fireDate;
//                localNotification.timeZone = [NSTimeZone defaultTimeZone];
//                localNotification.alertBody = @"朝のニュースが届きました";
//                localNotification.soundName = UILocalNotificationDefaultSoundName;
//                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//            }
        }
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
    fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    //通知内容
    dispatch_async(dispatch_get_main_queue(), ^{
        [notificattion_ notificatoinRemote:userInfo];
    });
    if(!_isSleepingOrBackground) {
        NSString *message = [userInfo valueForKey:@"message"];
        if(message && [message length] > 0) {
            //TODO フォアグラウンドでの処理
            NSString *debug = @"false";
#ifdef DEBUG
            debug = @"true";
#endif
            WKCMSViewController *vc = [self getRootWebViewController];
            [vc evaluatingJavaScript:[NSString stringWithFormat:@"setIds('%@', '%@', '%@', '%@', '%@', '%@');",
                                      [App getDeviceId], [App getUniqueId], @"iOS", [self iOSVerstion], [self applicationVersion], debug
                                      ] stopProgress:YES];

        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        //バイブレーション
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        //アラート
        AudioServicesPlaySystemSound(1002);
    });
    //
    if(completionHandler) {
        completionHandler(UIBackgroundFetchResultNoData);
    }
}

//- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
//{
//}


- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler
{
    //http://starzero.hatenablog.com/entry/2014/06/18/234327
//    [self addCompletionHandler:completionHandler forSession:identifier];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    //ローカル通知からの起動 -> バックグラウンド処理 記事処理
    if (application.applicationState == UIApplicationStateActive) {
        if (notification.userInfo != nil && notificattion_ != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [notificattion_ notificatoinLocal:notification.userInfo];
            });
        }
        return;
    }
    
    if (application.applicationState == UIApplicationStateInactive) {
        if (notification.userInfo != nil && notificattion_ != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [notificattion_ notificatoinLocal:notification.userInfo];
            });
        }
        return;
    }
}

// このメソッドで、プッシュ通知からの起動後の処理を行うことが出来る
- (void)onLaunchFromNotification:(NSString *)notificationsId message:(NSString *)message extra:(NSDictionary *)extra
{
    NSLog(@"ここでextraの中身にひもづいたインセンティブの付与などを行うことが出来ます");
    //通知内容
}

- (NSString*)iOSVerstion
{
    return [[UIDevice currentDevice] systemVersion];
}

- (NSString*)applicationVersion
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

- (BOOL)isUnderIOS9
{
    NSArray  *aOsVersions = [[[UIDevice currentDevice]systemVersion] componentsSeparatedByString:@"."];
    NSInteger iOsVersionMajor  = [[aOsVersions objectAtIndex:0] intValue];
    if (iOsVersionMajor < 9) {
        return YES;
    }
    
    return NO;
}

- (BOOL)isiPhone
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        return YES;
    }
    return NO;
}

- (NSUUID*)uuid
{
    return [[UIDevice currentDevice] identifierForVendor];
}

- (NSString*)documentRoot
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsRoot = [NSString stringWithFormat:@"%@/htdocs/", [paths objectAtIndex:0]];
    return documentsRoot;
}

- (NSString*)cacheRoot
{
    NSString *cacheRoot = [NSString stringWithFormat:@"%@/%@/", [self documentRoot], [self getConfigSetting:@"cachePath"]];
    return cacheRoot;
}

- (BOOL)hasDocumentRoot
{
    //TODO
    //TODO
    //TODO ドキュメントルートを入れ替える条件を変える
    //TODO
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL isDir = NO;
    BOOL exist = [fm fileExistsAtPath:[self documentRoot] isDirectory:&isDir];
    if(exist) {
        return YES;
    }
    
    return NO;
}

- (BOOL)checkUpdate
{
    NSString *latestVerstion = [self getSetting:LATEST_LAUNCH_VERSION] ? [NSString stringWithFormat:@"%@", [self getSetting:LATEST_LAUNCH_VERSION]] : nil;
    NSString *thisVersion = [self getVersionName];
    if(!latestVerstion || ![thisVersion isEqualToString:latestVerstion]) {
        return YES;
    }
    return NO;
}

- (void)setSetting:(NSString*)key value:(NSObject*)value
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:value forKey:key];
    [userDefaults synchronize];
}

- (NSObject*)getSetting:(NSString*)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:key];
}

- (void)removeSetting:(NSString*)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:key];
    [userDefaults synchronize];
}

- (void)setDeviceId:(NSObject*)value
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:value forKey:@"device_id"];
    [userDefaults synchronize];
}

- (NSString*)getDeviceId
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:@"device_id"];
}

- (void)setUniqueId:(NSObject*)value
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:value forKey:@"unique_id"];
    [userDefaults synchronize];
}

- (NSString*)getUniqueId
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:@"unique_id"];
}


- (NSString*) getConfigSetting:(NSString*)key
{
#ifdef DEBUG
    NSString *configPath = [NSString stringWithFormat:@"%@/appcms_config_debug.plist", [[NSBundle mainBundle] bundlePath]];
#else
    //本番サーバ
    NSString *configPath = [NSString stringWithFormat:@"%@/appcms_config.plist", [[NSBundle mainBundle] bundlePath]];
    //本番サーバ
#endif
    NSDictionary *config = [NSDictionary dictionaryWithContentsOfFile:configPath];
    return [config valueForKey:key];
}

- (NSString*) getAPIVersion
{
    return [self getConfigSetting:@"APIVersion"];
}

- (BOOL)isExportDomains:(NSURL*)url
{
    NSArray *domains = [[self getConfigSetting:@"exportDomains"] componentsSeparatedByString:@","];
    if(domains) {
        for(NSString *domain in domains) {
            NSString *host = [url host];
            if([host hasSuffix:domain]) {
                return YES;
            }
        }
    }
    
    return NO;
}

- (void)displayProgress
{
    if(!_isSleepingOrBackground && !pregoress_) {
        pregoress_ = YES;
    }
}

- (void)displayProgressWithView:(UIView*)view
{
    if(!_isSleepingOrBackground && !pregoress_) {
        pregoress_ = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            UIView *pview = self.progressView;
            [view addSubview:pview];
            [pview setTranslatesAutoresizingMaskIntoConstraints:NO];
            NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(pview);
            [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[pview]|"
                                                                         options:0
                                                                         metrics:nil
                                                                           views:viewsDictionary]
             ];
            [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[pview]|"
                                                                         options:0
                                                                         metrics:nil
                                                                           views:viewsDictionary]
             ];
            [view layoutIfNeeded];
        });
    }
}

- (void)hiddenProgress
{
    if(pregoress_) {
        pregoress_ = NO;
        // スプラッシュOFF
        [[NSNotificationCenter defaultCenter] postNotificationName:SPLASH_END_NOTIFICATION object:nil];
    }
}

- (void)setNotificationDelegate:(id<NotificationBroadcastDelegate>)notification
{
    notificattion_ = notification;
}

//backup対象から外す再帰処理
- (void)backupOffDir:(NSString*)path
{
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL isDir = NO;
    if([fm fileExistsAtPath:path isDirectory:&isDir]) {
        if(isDir) {
            [self setSkipBackupAttributeToItemAtPath:path];
            NSDirectoryEnumerator *dirEnum = [fm enumeratorAtPath:path];
            NSString *fileName;
            while (fileName = [dirEnum nextObject]) {
                NSString *fullPath = [path stringByAppendingPathComponent:fileName];
                [self backupOffDir:fullPath];
            }
        }else{
            [self setSkipBackupAttributeToItemAtPath:path];
        }
    }
}

//backup対象から外す
- (void)setSkipBackupAttributeToItemAtPath:(NSString *) filePathString
{
    NSURL* URL= [NSURL fileURLWithPath: filePathString];
    [[NSFileManager defaultManager] fileExistsAtPath: [URL path]];
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
}

- (NSString*)getLocalizedString:(NSString*)key
{
    return NSLocalizedString(key, @"");
}

- (NSString*)getVersion {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleVersion"];
}

- (NSString*)getVersionName {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}

- (WKCMSViewController*)getRootWebViewController
{
    UINavigationController *navi = (UINavigationController*)[self.window rootViewController];
    return (WKCMSViewController*)navi.topViewController;
}

- (NSString*)dateToStr:(NSDate*)date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"]];
    [df setDateFormat:@"yyyyMMdd'T'HHmmssZ"];
    return [df stringFromDate:date];
}

@end
