//
//  DownloadManager.m
//  AppCMS
//
//  Created by 長島 伸光 on 2015/01/03.
//  Copyright (c) 2015年 MEAS. All rights reserved.
//

#import "DownloadManager.h"
#import "CMSAppDelegate.h"
#import "WKCMSViewController.h"
#import <AFNetworking/AFNetworking.h>
#import <SystemConfiguration/SCNetworkReachability.h>
#import <SSZipArchive/SSZipArchive.h>
#import <CommonCrypto/CommonDigest.h>

@interface DownloadManager() {
    NSMutableDictionary *downloadMap_;
    NSMutableDictionary *onlineWaitMap_;
    NSMutableDictionary *backgroundTask_;
    BOOL taskWorking_;
    NETWORK_STATUS status_;
    updateUI initialCallback_;
    
    NSMutableData *responseData_;
}

@end

static DownloadManager *sharedInstance;

@implementation DownloadManager

@synthesize initilaLock_;

+ (DownloadManager*)buildManager
{
    
    static dispatch_once_t once;
    dispatch_once( &once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    //TODO:ダウンロードが行われていないという条件
//    sharedInstance.initilaLock_ = NO;
    [sharedInstance removeHeader];
    
    return sharedInstance;
    
}

+ (id)allocWithZone:(NSZone *)zone
{
    __block id ret = nil;
    
    static dispatch_once_t once;
    dispatch_once( &once, ^{
        sharedInstance = [super allocWithZone:zone];
        ret = sharedInstance;
    });
    
    return  ret;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (id)init
{
    self = [super init];
    if(self) {
        initilaLock_ = NO;
        if(!downloadMap_) {
            downloadMap_ = [[NSMutableDictionary alloc] initWithCapacity:0];
        }
        if(!onlineWaitMap_) {
            onlineWaitMap_ = [[NSMutableDictionary alloc] initWithCapacity:0];
        }
        if(!backgroundTask_) {
            backgroundTask_ = [[NSMutableDictionary alloc] initWithCapacity:0];
        }
        taskWorking_ = NO;
    }
    return self;
}

- (NETWORK_STATUS)checkNetwork
{
    status_ = CONNECT_WAIT;
    if([NSRunLoop currentRunLoop] == [NSRunLoop mainRunLoop]) {
        [self checkNetworkRun];
    }else{
        NSMutableArray *modeArray = [[NSMutableArray alloc] initWithObjects:NSDefaultRunLoopMode, nil];
        [[NSRunLoop mainRunLoop] performSelector:@selector(checkNetworkRun) target:self argument:nil order:0 modes:modeArray];
        while (status_ == CONNECT_WAIT) {
            [NSThread sleepForTimeInterval:0.02];
        }
        [modeArray removeAllObjects];
        modeArray = nil;
    }
    return status_;
}

- (void)checkNetworkRun
{
    [self checkNetworkRun:NO];
//    [self checkNetworkRun:YES]; //2〜3秒遅くなる
}

- (void)checkNetworkRun:(BOOL)deap
{
    Reachability *reachability = [Reachability buildObject];
    if([reachability waitReachable]) {
        status_ = NOT_CONNECT;
        return;
    }
    
    //2015.07.09 ONLINEチェック
    if(deap && ![self onlineAccessable])  {
        status_ = NOT_CONNECT;
        return;
    }
    
    //Network configuration check
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityRef reachabilityRef;
    reachabilityRef =  SCNetworkReachabilityCreateWithName(NULL, REACHABLE_CHECK_HOSTNAME);
    if(reachabilityRef == NULL) {
        status_ = NOT_CONNECT;
        return;
    }
    BOOL gotFlags = SCNetworkReachabilityGetFlags(reachabilityRef, &flags);
    CFRelease(reachabilityRef);
    BOOL cellConnect = NO;
    
    if (!gotFlags) {
        status_ = NOT_CONNECT;
        return;
    }
    
    BOOL isReachable = flags & kSCNetworkReachabilityFlagsReachable;
    
    BOOL noConnectionRequired = !(flags & kSCNetworkReachabilityFlagsConnectionRequired);
    if ((flags & kSCNetworkReachabilityFlagsIsWWAN)) {
        cellConnect = YES;
        noConnectionRequired = YES;
    }
    
    if(!isReachable || !noConnectionRequired) {
        status_ = NOT_CONNECT;
        return;
    }
    
    status_ = cellConnect ? CONNECT_3G : CONNECT;
    
    return;
}

- (BOOL)onlineAccessable {
    BOOL result = YES;
    
    NSURL *testURL = [[NSURL alloc] initWithString:ACCESSABLE_CHECK_URL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:testURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0];
    NSHTTPURLResponse *response;
    NSError *nserr;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&nserr];
    if(!data) {
        result = NO;
    }
    if(nserr) {
        NSLog(@"deep error >> %@", nserr.localizedDescription);
        result = NO;
    }
    
    return result;
}


- (NSURL*)exchangeToLocalCacheURL:(NSURL*)cacheURL
{
    if(cacheURL) {
        NSString *absPath = [cacheURL absoluteString];
        NSString *path = [cacheURL path];
        NSString *cachePath = [App getConfigSetting:@"cachePath"];
        NSString *baseURL = [absPath substringToIndex:[absPath rangeOfString:path].location];
        
        return [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@", baseURL, cachePath, path]];
    }
    return nil;
}

- (void)addBackgroundTask:(NSString*)key task:(id<backgroundTask>)task
{
    [backgroundTask_ setValue:task forKey:key];
}

- (void)removeBackgroundTask:(NSString*)key
{
    [backgroundTask_ removeObjectForKey:key];
}

- (void)downloadBackground:(updateUI)succeed
{
    if(!taskWorking_) {
        taskWorking_ = YES;
        [self backgroundTaskExec:0 callback:succeed];
    }
}

- (void)backgroundTaskExec:(int)idx callback:(updateUI)succeed
{
    NSArray *keys = [backgroundTask_ allKeys];
    if(idx < [keys count]) {
        NSString *key = [keys objectAtIndex:idx];
        id<backgroundTask> task = [backgroundTask_ valueForKey:key];
        [task execTask:^(BOOL sccess) {
            [self backgroundTaskExec:idx+1 callback:succeed];
        }];
    }else{
        succeed(YES);
        taskWorking_ = NO;
    }
}

- (void)download:(NSURL*)url succeed:(downloadSucceeded)succeed failed:(downloadFailed)failed
{
    [self download:url succeed:succeed failed:failed onProgress:YES];
}

- (void)downloadUIPackage:(updateUI)update
{
    [self downloadUIPackage:update onProgress:YES];
}

- (void)downloadWithCache:(NSURL*)url cache:(NSURL*)cacheURL succeed:(downloadSucceeded)succeed failed:(downloadFailed)failed
{
    [self downloadWithCache:url cache:cacheURL succeed:succeed failed:failed onProgress:YES];
}

- (void)download:(NSURL*)url datas:(NSDictionary*)datas succeed:(downloadSucceeded)succeed failed:(downloadFailed)failed
{
    [self download:url datas:datas succeed:succeed failed:failed onProgress:YES];
}

- (void)downloadWithCache:(NSURL*)url datas:(NSDictionary*)datas cache:(NSURL*)cacheURL succeed:(downloadSucceeded)succeed failed:(downloadFailed)failed
{
    [self downloadWithCache:url datas:datas cache:cacheURL succeed:succeed failed:failed onProgress:YES];
}

- (void)offProgressDownload:(NSURL*)url succeed:(downloadSucceeded)succeed failed:(downloadFailed)failed
{
    [self download:url succeed:succeed failed:failed onProgress:NO];
}

- (void)offProgressDownloadUIPackage:(updateUI)update
{
    [self downloadUIPackage:update onProgress:NO];
}

- (void)offProgressDownloadWithCache:(NSURL*)url cache:(NSURL*)cacheURL succeed:(downloadSucceeded)succeed failed:(downloadFailed)failed
{
    [self downloadWithCache:url cache:cacheURL succeed:succeed failed:failed onProgress:NO];
}

- (void)offProgressDownload:(NSURL*)url datas:(NSDictionary*)datas succeed:(downloadSucceeded)succeed failed:(downloadFailed)failed
{
    [self download:url datas:datas succeed:succeed failed:failed onProgress:NO];
}

- (void)offProgressDownload:(NSURL*)url datas:(NSDictionary*)datas succeed:(downloadSucceeded)succeed failed:(downloadFailed)failed method:(NSString*)method;
{
    [self download:url datas:datas succeed:succeed failed:failed method:method onProgress:NO];
}

- (void)offProgressDownloadWithCache:(NSURL*)url datas:(NSDictionary*)datas cache:(NSURL*)cacheURL succeed:(downloadSucceeded)succeed failed:(downloadFailed)failed
{
    [self downloadWithCache:url datas:datas cache:cacheURL succeed:succeed failed:failed onProgress:NO];
}

- (NSURLSessionConfiguration*)createSessionConfig:(NSString*)identifier
{
    NSURLSessionConfiguration *configration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:identifier];
    configration.allowsCellularAccess = YES;
    if(![App isSleepingOrBackground]) {
        configration.timeoutIntervalForRequest = 0;
        configration.timeoutIntervalForResource = 60;
        configration.discretionary = NO;
    }
    return configration;
}

- (void)appedHedaer:(NSMutableURLRequest*)request
{
    if(header_) {
        NSArray *keys = [header_ allKeys];
        for(NSString *key in keys) {
            NSString *value = [header_ valueForKey:key];
            [request addValue:value forHTTPHeaderField:key];
        }
    }
}

- (void)download:(NSURL*)url succeed:(downloadSucceeded)succeed failed:(downloadFailed)failed onProgress:(BOOL)onPregress
{
    if(onPregress) {
        [App displayProgress];
    }
    NETWORK_STATUS nstatus = [self checkNetwork];
    if(nstatus != CONNECT && nstatus != CONNECT_3G) {
        NSError *error = [[NSError alloc] initWithDomain:[url absoluteString] code:500
                                                userInfo:@{NSLocalizedDescriptionKey:[NSHTTPURLResponse localizedStringForStatusCode:500],
                                                           NSLocalizedRecoverySuggestionErrorKey:[NSHTTPURLResponse localizedStringForStatusCode:500]}];
        failed(error, url);
        if(onPregress) {
            [App hiddenProgress];
        }
    }else{
//        dispatch_queue_t sQueue = dispatch_get_main_queue();
        dispatch_queue_t sQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(sQueue, ^{
            NSString *identifier = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
            NSURLSessionConfiguration *configration = [self createSessionConfig:identifier];
            NSURLSession *session = [NSURLSession sessionWithConfiguration:configration delegate:self delegateQueue:nil];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
            [request setTimeoutInterval:REQUEST_TIMEOUT];
            [request setHTTPShouldHandleCookies:YES];
            [request setHTTPMethod:@"GET"];
            [self appedHedaer:request];
            NSURLSessionDownloadTask* downloadTask = [session downloadTaskWithRequest:request];
            CallbackObject *callback = [[CallbackObject alloc] init];
            callback.onProgress = onPregress;
            [callback setCallback:succeed failed:failed];
            [downloadMap_ setObject:callback forKey:identifier];
            [downloadTask resume];
        });
    }
}

- (void)downloadUIPackage:(updateUI)update onProgress:(BOOL)onPregress
{
    if(onPregress) {
        [App displayProgress];
    }
    NETWORK_STATUS nstatus = [self checkNetwork];
    if(nstatus == CONNECT || nstatus == CONNECT_3G) {
//        dispatch_queue_t sQueue = dispatch_get_main_queue();
        dispatch_queue_t sQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(sQueue, ^{
            NSString *identifier = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
            NSURLSessionConfiguration *configration = [self createSessionConfig:identifier];
            NSURLSession *session = [NSURLSession sessionWithConfiguration:configration delegate:self delegateQueue:nil];
            NSURL *url = [NSURL URLWithString:[App getConfigSetting:@"UIPackageURL"]];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            NSURLSessionDownloadTask* downloadTask = [session downloadTaskWithRequest:request];
            CallbackObject *callback = [[CallbackObject alloc] init];
            callback.onProgress = onPregress;
            [callback setCallback:update];
            [downloadMap_ setObject:callback forKey:identifier];
            [downloadTask resume];
        });
    }else{
        update(NO);
        if(onPregress) {
            [App hiddenProgress];
        }
    }
}

- (void)downloadWithCache:(NSURL*)url cache:(NSURL*)cacheURL succeed:(downloadSucceeded)succeed failed:(downloadFailed)failed onProgress:(BOOL)onPregress
{
    if(onPregress) {
        [App displayProgress];
    }
    NETWORK_STATUS nstatus = [self checkNetwork];
    NSURL *cacheURLA = [self exchangeToLocalCacheURL:cacheURL];
    
    if(nstatus != CONNECT && nstatus != CONNECT_3G) {
        NSString *cacheRealPath = [NSString stringWithFormat:@"%@%@", [App cacheRoot], [cacheURL path]];
        NSFileManager *fm = [NSFileManager defaultManager];
        if([fm fileExistsAtPath:cacheRealPath]) {
            succeed(cacheURLA);
        }else{
            CallbackObject *callback = [[CallbackObject alloc] init];
            callback.onProgress = onPregress;
            [callback setCallback:succeed failed:failed cache:cacheURLA];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
            [request setTimeoutInterval:REQUEST_TIMEOUT];
            [request setHTTPShouldHandleCookies:YES];
            [request setHTTPMethod:@"GET"];
            [self appedHedaer:request];
            [onlineWaitMap_ setObject:callback forKey:request];
        }
        if(onPregress) {
            [App hiddenProgress];
        }
    }else{
//        dispatch_queue_t sQueue = dispatch_get_main_queue();
        dispatch_queue_t sQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(sQueue, ^{
            NSString *identifier = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
            NSURLSessionConfiguration *configration = [self createSessionConfig:identifier];
            NSURLSession *session = [NSURLSession sessionWithConfiguration:configration delegate:self delegateQueue:nil];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
            [request setTimeoutInterval:REQUEST_TIMEOUT];
            [request setHTTPShouldHandleCookies:YES];
            [request setHTTPMethod:@"GET"];
            [self appedHedaer:request];
            NSURLSessionDownloadTask* downloadTask = [session downloadTaskWithRequest:request];
            CallbackObject *callback = [[CallbackObject alloc] init];
            callback.onProgress = onPregress;
            [callback setCallback:succeed failed:failed cache:cacheURLA];
            [downloadMap_ setObject:callback forKey:identifier];
            [downloadTask resume];
        });
    }
}

- (void)download:(NSURL*)url datas:(NSDictionary*)datas succeed:(downloadSucceeded)succeed failed:(downloadFailed)failed onProgress:(BOOL)onPregress
{
    [self download:url datas:datas succeed:succeed failed:failed method:@"POST" onProgress:onPregress];
}

- (void)download:(NSURL*)url datas:(NSDictionary*)datas succeed:(downloadSucceeded)succeed failed:(downloadFailed)failed method:(NSString*)method onProgress:(BOOL)onPregress
{
    if(onPregress) {
        [App displayProgress];
    }
    NETWORK_STATUS nstatus = [self checkNetwork];
    if(nstatus != CONNECT && nstatus != CONNECT_3G) {
        NSError *error = [[NSError alloc] initWithDomain:[url absoluteString] code:500
                                                userInfo:@{NSLocalizedDescriptionKey:[NSHTTPURLResponse localizedStringForStatusCode:500],
                                                           NSLocalizedRecoverySuggestionErrorKey:[NSHTTPURLResponse localizedStringForStatusCode:500]}];
        failed(error, url);
        if(onPregress) {
            [App hiddenProgress];
        }
    }else{
        //        dispatch_queue_t sQueue = dispatch_get_main_queue();
        dispatch_queue_t sQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(sQueue, ^{
            NSString *identifier = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
            NSURLSessionConfiguration *configration = [self createSessionConfig:identifier];
            NSURLSession *session = [NSURLSession sessionWithConfiguration:configration delegate:self delegateQueue:nil];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:method];
            if(datas) {
                NSMutableData *sendData = [NSMutableData data];
                NSArray *keys = [datas allKeys];
                for(NSString *key in keys) {
                    if([keys count] > 1) {
                        [sendData appendData:[[NSString stringWithFormat:@"--%@\r\n", MULTIPART_BOUNDALY] dataUsingEncoding:NSUTF8StringEncoding]];
                    }
                    NSObject *data = [datas valueForKey:key];
                    if([data isKindOfClass:[NSData class]]) {
                        [sendData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data;"] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"name=\"%@\";", key] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"filename=\"%@.jpg\"\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"Content-Type: image/jpeg\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:(NSData*)data];
                        [sendData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                    }else if([data isKindOfClass:[UIImage class]]) {
                        [sendData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data;"] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"name=\"%@\";", key] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"filename=\"%@.png\"\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"Content-Type: image/png\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:UIImagePNGRepresentation((UIImage*)data)];
                        [sendData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                    }else{
                        if([keys count] > 1) {
                            [sendData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data;"] dataUsingEncoding:NSUTF8StringEncoding]];
                        }
                        if([key length] > 0) {
                            [sendData appendData:[[NSString stringWithFormat:@"name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
                        }else{
                            //JSONパースしてチェック
                            if([data isKindOfClass:[NSString class]]) {
                                NSError *error;
                                [NSJSONSerialization JSONObjectWithData:[(NSString*)data dataUsingEncoding:NSUTF8StringEncoding]
                                                                options:NSJSONReadingMutableContainers error:&error];
                                if(!error) {
                                    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
                                    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
                                    [request setValue:[NSString stringWithFormat:@"%luld", (unsigned long)sendData.length] forHTTPHeaderField:@"Content-Length"];
                                }
                            }
                            [sendData appendData:[@"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                        }
                        [sendData appendData:[[NSString stringWithFormat:@"%@\r\n", data] dataUsingEncoding:NSUTF8StringEncoding]];
                    }
                    
                }
                if([keys count] > 1) {
                    [sendData appendData:[[NSString stringWithFormat:@"--%@--\r\n", MULTIPART_BOUNDALY] dataUsingEncoding:NSUTF8StringEncoding]];
                }
                [self appedHedaer:request];
                [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
                [request setTimeoutInterval:REQUEST_TIMEOUT];
                [request setHTTPShouldHandleCookies:YES];
                [request setHTTPBody:sendData];
            }
            NSURLSessionDownloadTask* downloadTask = [session downloadTaskWithRequest:request];
            CallbackObject *callback = [[CallbackObject alloc] init];
            callback.onProgress = onPregress;
            [callback setCallback:succeed failed:failed];
            [downloadMap_ setObject:callback forKey:identifier];
            [downloadTask resume];
        });
    }
}

- (void)downloadWithCache:(NSURL*)url datas:(NSDictionary*)datas cache:(NSURL*)cacheURL
                  succeed:(downloadSucceeded)succeed failed:(downloadFailed)failed onProgress:(BOOL)onPregress
{
    if(onPregress) {
        [App displayProgress];
    }
    NETWORK_STATUS nstatus = [self checkNetwork];
    NSURL *cacheURLA = [self exchangeToLocalCacheURL:cacheURL];
    
    if(nstatus != CONNECT && nstatus != CONNECT_3G) {
        NSFileManager *fm = [NSFileManager defaultManager];
        NSString *cacheRealPath = [NSString stringWithFormat:@"%@%@", [App cacheRoot], [cacheURL path]];
        if([fm fileExistsAtPath:cacheRealPath]) {
            succeed(cacheURLA);
            if(onPregress) {
                [App hiddenProgress];
            }
        }else{
            CallbackObject *callback = [[CallbackObject alloc] init];
            callback.onProgress = onPregress;
            [callback setCallback:succeed failed:failed cache:cacheURLA];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            if(datas) {
                NSMutableData *sendData = [NSMutableData data];
                NSArray *keys = [datas allKeys];
                for(NSString *key in keys) {
                    if([keys count] > 1) {
                        [sendData appendData:[[NSString stringWithFormat:@"--%@\r\n", MULTIPART_BOUNDALY] dataUsingEncoding:NSUTF8StringEncoding]];
                    }
                    NSObject *data = [datas valueForKey:key];
                    if([data isKindOfClass:[NSData class]]) {
                        [sendData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data;"] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"name=\"%@\";", key] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"filename=\"%@.jpg\"\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"Content-Type: image/jpeg\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:(NSData*)data];
                        [sendData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                    }else if([data isKindOfClass:[UIImage class]]) {
                        [sendData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data;"] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"name=\"%@\";", key] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"filename=\"%@.png\"\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"Content-Type: image/png\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:UIImagePNGRepresentation((UIImage*)data)];
                        [sendData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                    }else{
                        if([keys count] > 1) {
                            [sendData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data;"] dataUsingEncoding:NSUTF8StringEncoding]];
                        }
                        if([key length] > 0) {
                            [sendData appendData:[[NSString stringWithFormat:@"name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
                        }else{
                            [sendData appendData:[@"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                        }
                        [sendData appendData:[[NSString stringWithFormat:@"%@\r\n", data] dataUsingEncoding:NSUTF8StringEncoding]];
                    }
                    
                }
                if([keys count] > 1) {
                    [sendData appendData:[[NSString stringWithFormat:@"--%@--\r\n", MULTIPART_BOUNDALY] dataUsingEncoding:NSUTF8StringEncoding]];
                }
                [self appedHedaer:request];
                [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
                [request setTimeoutInterval:REQUEST_TIMEOUT];
                [request setHTTPShouldHandleCookies:YES];
                [request setHTTPBody:sendData];
            }
            [onlineWaitMap_ setObject:callback forKey:request];
        }
    }else{
//        dispatch_queue_t sQueue = dispatch_get_main_queue();
        dispatch_queue_t sQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(sQueue, ^{
            NSString *identifier = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
            NSURLSessionConfiguration *configration = [self createSessionConfig:identifier];
            NSURLSession *session = [NSURLSession sessionWithConfiguration:configration delegate:self delegateQueue:nil];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            if(datas) {
                NSMutableData *sendData = [NSMutableData data];
                NSArray *keys = [datas allKeys];
                for(NSString *key in keys) {
                    if([keys count] > 1) {
                        [sendData appendData:[[NSString stringWithFormat:@"--%@\r\n", MULTIPART_BOUNDALY] dataUsingEncoding:NSUTF8StringEncoding]];
                    }
                    NSObject *data = [datas valueForKey:key];
                    if([data isKindOfClass:[NSData class]]) {
                        [sendData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data;"] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"name=\"%@\";", key] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"filename=\"%@.jpg\"\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"Content-Type: image/jpeg\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:(NSData*)data];
                        [sendData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                    }else if([data isKindOfClass:[UIImage class]]) {
                        [sendData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data;"] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"name=\"%@\";", key] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"filename=\"%@.png\"\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:[[NSString stringWithFormat:@"Content-Type: image/png\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                        [sendData appendData:UIImagePNGRepresentation((UIImage*)data)];
                        [sendData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                    }else{
                        if([keys count] > 1) {
                            [sendData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data;"] dataUsingEncoding:NSUTF8StringEncoding]];
                        }
                        if([key length] > 0) {
                            [sendData appendData:[[NSString stringWithFormat:@"name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
                        }else{
                            [sendData appendData:[@"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                        }
                        [sendData appendData:[[NSString stringWithFormat:@"%@\r\n", data] dataUsingEncoding:NSUTF8StringEncoding]];
                    }
                    
                }
                if([keys count] > 1) {
                    [sendData appendData:[[NSString stringWithFormat:@"--%@--\r\n", MULTIPART_BOUNDALY] dataUsingEncoding:NSUTF8StringEncoding]];
                }
                [self appedHedaer:request];
                [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
                [request setTimeoutInterval:REQUEST_TIMEOUT];
                [request setHTTPShouldHandleCookies:YES];
                [request setHTTPBody:sendData];
            }
            NSURLSessionDownloadTask* downloadTask = [session downloadTaskWithRequest:request];
            CallbackObject *callback = [[CallbackObject alloc] init];
            callback.onProgress = onPregress;
            [callback setCallback:succeed failed:failed cache:cacheURLA];
            [downloadMap_ setObject:callback forKey:identifier];
            [downloadTask resume];
        });
    }
}


#pragma ReachabilityDelegate protocol

- (void)reachNetwork
{
    if(onlineWaitMap_) {
        NSArray *keys = [onlineWaitMap_ allKeys];
        for(NSMutableURLRequest *request in keys) {
            dispatch_queue_t sQueue = dispatch_get_main_queue();
            dispatch_async(sQueue, ^{
                NSString *identifier = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
                NSURLSessionConfiguration *configration = [self createSessionConfig:identifier];
                NSURLSession *session = [NSURLSession sessionWithConfiguration:configration delegate:self delegateQueue:nil];
                NSURLSessionDownloadTask* downloadTask = [session downloadTaskWithRequest:request];
                CallbackObject *callback = [onlineWaitMap_ objectForKey:request];
                callback.onProgress = NO;
                [downloadMap_ setObject:callback forKey:identifier];
                [downloadTask resume];
            });
        }
    }
    [onlineWaitMap_ removeAllObjects];
}

#pragma NSURLSessionDownloadDelegate protocol

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten
                                            totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    CallbackObject *callback = [downloadMap_ objectForKey:session.configuration.identifier];
    if(callback) {
        if([callback getType] == UI_UPDATE) {
            NSHTTPURLResponse *response = (NSHTTPURLResponse*)downloadTask.response;
            if(![self isNewUIPack:response.allHeaderFields settingUpdate:NO]) {
                //更新がないので終了
                NSString *lastMod = [response.allHeaderFields objectForKey:@"ETag"];
                NSLog(@"Same UI Package Download Cancel(%@)", lastMod);
                [downloadTask cancel];
                updateUI succeed = [callback getUpdateUI];
                succeed(NO);
                [downloadMap_ removeObjectForKey:session.configuration.identifier];
            }else{
                if(callback.onProgress) { //2015.03.30 プログレスが消えない障害
                    [App displayProgress];
                }
            }
        }
    }
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    //downloadファイルパス
    CallbackObject *callback = [downloadMap_ objectForKey:session.configuration.identifier];
    NSFileManager *fm = [NSFileManager defaultManager];
    if(callback) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse*)downloadTask.response;
        if([callback getType] == DOWNLOAD) {
            if(response.statusCode == 200) {
                downloadSucceeded succeed = [callback getSucceed];
                succeed(location);
            }else{
                downloadFailed failed = [callback getFailed];
                NSError *error = [[NSError alloc] initWithDomain:[response.URL absoluteString] code:response.statusCode
                                                        userInfo:@{NSLocalizedDescriptionKey:[NSHTTPURLResponse localizedStringForStatusCode:response.statusCode],
                                                                   NSLocalizedRecoverySuggestionErrorKey:[NSHTTPURLResponse localizedStringForStatusCode:response.statusCode]}];
                failed(error, response.URL);
                if([fm fileExistsAtPath:[location path]]) {
                    [fm removeItemAtPath:[location path] error:nil];
                }
            }
        }else if([callback getType] == DOWNLOAD_CACHE) {
            if(response.statusCode == 200) {
                if([callback getCachePath]) {
                    NSString *cacheRealPath = [NSString stringWithFormat:@"%@%@", [App documentRoot], [[callback getCachePath] path]];
                    if([[response.MIMEType lowercaseString] rangeOfString:@"zip"].location != NSNotFound) {
                        if([fm fileExistsAtPath:cacheRealPath]) {
                            [fm removeItemAtPath:cacheRealPath error:nil];
                        }
                        if([SSZipArchive unzipFileAtPath:[location path] toDestination:cacheRealPath]) {
                            [fm removeItemAtPath:[location path] error:nil];
                            //解凍ファイルをバックアップ対象外に
                            [App backupOffDir:cacheRealPath];
                            downloadSucceeded succeed = [callback getSucceed];
                            succeed([callback getCachePath]);
                        }else{
                            downloadFailed failed = [callback getFailed];
                            NSError *error = [[NSError alloc] initWithDomain:[[callback getCachePath] absoluteString] code:500
                                                                    userInfo:@{NSLocalizedDescriptionKey:[NSHTTPURLResponse localizedStringForStatusCode:500],
                                                                               NSLocalizedRecoverySuggestionErrorKey:[NSHTTPURLResponse localizedStringForStatusCode:500]}];
                            failed(error, response.URL);
                        }
                    }else{
                        NSError *error = nil;
                        if([fm fileExistsAtPath:cacheRealPath]) {
                            [fm removeItemAtPath:cacheRealPath error:nil];
                        }
                        [fm moveItemAtPath:[location path] toPath:cacheRealPath error:&error];
                        if(error) {
                            downloadFailed failed = [callback getFailed];
                            failed(error, response.URL);
                        }else{
                            //解凍ファイルをバックアップ対象外に
                            [App backupOffDir:cacheRealPath];
                            downloadSucceeded succeed = [callback getSucceed];
                            succeed([callback getCachePath]);
                        }
                    }
                }else{
                    downloadSucceeded succeed = [callback getSucceed];
                    succeed([callback getCachePath]);
                }
                if([fm fileExistsAtPath:[location path]]) {
                    [fm removeItemAtPath:[location path] error:nil];
                }
            }else{
                downloadFailed failed = [callback getFailed];
                NSError *error = [[NSError alloc] initWithDomain:[response.URL absoluteString] code:response.statusCode
                                                        userInfo:@{NSLocalizedDescriptionKey:[NSHTTPURLResponse localizedStringForStatusCode:response.statusCode],
                                                                   NSLocalizedRecoverySuggestionErrorKey:[NSHTTPURLResponse localizedStringForStatusCode:response.statusCode]}];
                failed(error, response.URL);
            }
        }else if([callback getType] == UI_UPDATE) {
            updateUI succeed = [callback getUpdateUI];
            BOOL call = NO;
            if(response.statusCode == 200) {
                if([self isNewUIPack:response.allHeaderFields settingUpdate:YES]) {
                    if([[response.MIMEType lowercaseString] rangeOfString:@"zip"].location != NSNotFound) {
                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                        NSString *documentsRootTemp = [NSString stringWithFormat:@"%@/htdocs_tmp/", [paths objectAtIndex:0]];
                        NSString *cacheRootTemp = [NSString stringWithFormat:@"%@%@", documentsRootTemp, [App getConfigSetting:@"cachePath"]];

                        if([SSZipArchive unzipFileAtPath:[location path] toDestination:documentsRootTemp]) {
                            NSError *error = nil;
                            //キャッシュファイルをリストア
                            if([fm fileExistsAtPath:[App cacheRoot]]) {
                                [fm moveItemAtPath:[App cacheRoot] toPath:cacheRootTemp error:&error];
                                if(error) {
                                    NSLog(@"%@", error.localizedDescription);
                                }
                            }
                            
                            NSString *documentsRoot = [App documentRoot];
                            [fm removeItemAtPath:documentsRoot error:&error];
                            [fm moveItemAtPath:documentsRootTemp toPath:documentsRoot error:&error];
                            [fm removeItemAtPath:[location path] error:nil];
                            call = YES;
                            //ドキュメントルートをバックアップ対象外に
                            [App backupOffDir:documentsRoot];
                        }
                    }
                }else{
                    [fm removeItemAtPath:[location path] error:nil];
                }
                if([fm fileExistsAtPath:[location path]]) {
                    [fm removeItemAtPath:[location path] error:nil];
                }
            }
            if(call) {
                succeed(YES);
            }else{
                succeed(NO);
            }
        }
        [downloadMap_ removeObjectForKey:session.configuration.identifier];
        if(callback.onProgress) {
            [App hiddenProgress];
        }
    }else{
        if([fm fileExistsAtPath:[location path]]) {
            [fm removeItemAtPath:[location path] error:nil];
        }
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    CallbackObject *callback = [downloadMap_ objectForKey:session.configuration.identifier];
    NSHTTPURLResponse *response = (NSHTTPURLResponse*)task.response;
    if(response.statusCode != 200) {
        if(callback) {
            if([callback getType] == UI_UPDATE) {
                updateUI update = [callback getUpdateUI];
                if(update) {
                    update(NO);
                }
            }else{
                downloadFailed failed = [callback getFailed];
                if(failed) {
                    failed(error, response.URL);
                }
            }
        }
    }
    if(callback.onProgress) {
        [App hiddenProgress];
    }
}

- (BOOL)isNewUIPack:(NSDictionary*)header settingUpdate:(BOOL)settingUpdate
{
    NSString *lastMod = [header objectForKey:@"ETag"];
    NSString *lastUpdate = (NSString*)[App getSetting:UI_LASTUPDATE];
#ifdef DEBUG
    NSLog(@"-- check UIPAck %@ = %@", lastMod, lastUpdate);
#endif
    if(lastUpdate && [lastUpdate isEqualToString:lastMod]) {
        return NO;
    }
    if(settingUpdate) {
        NSLog(@"check UIPAck %@ = %@", lastMod, lastUpdate);
        [App setSetting:UI_LASTUPDATE value:lastMod];
    }
    return YES;
}

///
// 非推奨、ただし速い
///
- (void)initialDownload:(updateUI)succeed
{
    NETWORK_STATUS nstatus = [self checkNetwork];
    if(nstatus == CONNECT || nstatus == CONNECT_3G) {
        NSURL *url = [NSURL URLWithString:[App getConfigSetting:@"UIPackageURL"]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:REQUEST_TIMEOUT];
        [request setHTTPMethod:@"HEAD"];
        [request setTimeoutInterval:REQUEST_TIMEOUT];
        NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:mainQueue
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   NSHTTPURLResponse *httpRes = (NSHTTPURLResponse*)response;
                                   if(!error && httpRes.statusCode == 200) {
                                       
                                       if(![self isNewUIPack:httpRes.allHeaderFields settingUpdate:YES]) {
                                           //更新がないので終了
                                           NSString *lastMod = [httpRes.allHeaderFields objectForKey:@"ETag"];
                                           NSLog(@"Same UI Package No Update(%@)", lastMod);
                                           succeed(YES);
                                       }else{
                                           [self initialDownloadGet:succeed];
                                       }
                                   }
                               }];
    }else{
        NSFileManager *fm = [NSFileManager defaultManager];
        if(![fm fileExistsAtPath:[App cacheRoot]]) { //キャッシュがない
            dispatch_async(dispatch_get_main_queue(), ^{
                [self initialDownloadErrorLoop:succeed];
            });
        }else{
            //ローカルで起動
            dispatch_async(dispatch_get_main_queue(), ^{
                succeed(YES);
            });
        }
   }
}

- (void)initialDownloadGet:(updateUI)succeed
{
    [App displayProgress]; //表示する
    // スプラッシュON待機
    [[NSNotificationCenter defaultCenter] postNotificationName:SPLASH_START_NOTIFICATION object:nil];
    NETWORK_STATUS nstatus = [self checkNetwork];
    if(!initilaLock_ && (nstatus == CONNECT || nstatus == CONNECT_3G)) {
        initilaLock_= YES;
        NSURL *url = [NSURL URLWithString:[App getConfigSetting:@"UIPackageURL"]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:REQUEST_TIMEOUT];
        [request setHTTPMethod:@"GET"];
        [request setTimeoutInterval:REQUEST_TIMEOUT];
        NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:mainQueue
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   NSHTTPURLResponse *httpRes = (NSHTTPURLResponse*)response;
                                   if(!error && httpRes.statusCode == 200) {

                                       if([[httpRes.MIMEType lowercaseString] rangeOfString:@"zip"].location != NSNotFound) {
                                           NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                           NSString *documentsRootTemp = [NSString stringWithFormat:@"%@/htdocs_tmp/", [paths objectAtIndex:0]];
                                           NSString *cacheRootTemp = [NSString stringWithFormat:@"%@%@", documentsRootTemp, [App getConfigSetting:@"cachePath"]];
                                           
                                           NSString *tmpZipPath = [NSString stringWithFormat:@"%@/temp.zip", NSTemporaryDirectory()];
                                           [data writeToFile:tmpZipPath atomically:YES];
                                           if([SSZipArchive unzipFileAtPath:tmpZipPath toDestination:documentsRootTemp]) {
                                               NSFileManager *fm = [NSFileManager defaultManager];
                                               NSError *error = nil;
                                               //キャッシュファイルをリストア
                                               if([fm fileExistsAtPath:[App cacheRoot]]) {
                                                   [fm moveItemAtPath:[App cacheRoot] toPath:cacheRootTemp error:&error];
                                                   if(error) {
                                                       NSLog(@"%@", error.localizedDescription);
                                                   }
                                               }
                                               
                                               NSString *documentsRoot = [App documentRoot];
                                               [fm removeItemAtPath:documentsRoot error:&error];
                                               [fm moveItemAtPath:documentsRootTemp toPath:documentsRoot error:&error];
                                               [fm removeItemAtPath:tmpZipPath error:nil];
                                               //ドキュメントルートをバックアップ対象外に
                                               [App backupOffDir:documentsRoot];
                                           }
                                       }
                                   }
                                   succeed(YES);
                               }];
    }else{
        NSFileManager *fm = [NSFileManager defaultManager];
        if(![fm fileExistsAtPath:[App cacheRoot]]) { //キャッシュがない
            dispatch_async(dispatch_get_main_queue(), ^{
                [self initialDownloadErrorLoop:succeed];
            });
        }else{
            //ローカルで起動
            dispatch_async(dispatch_get_main_queue(), ^{
                succeed(YES);
            });
        }
        initilaLock_ = NO;
    }
}

- (void)initialDownloadErrorLoop:(updateUI)succeed
{
    initialCallback_ = succeed;
    UIAlertController *alert = nil;
    alert = [UIAlertController alertControllerWithTitle:@""
                                                message:@"インストール時はコンテンツのダウンロードが必要です。\nネットワークを確認して再度ダウンロードしてください。"
                                         preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"もう一度ダウンロードする"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction *action) {
                                                [self initialDownload:initialCallback_];
                                            }]];
    [[App getRootWebViewController] presentViewController:alert animated:YES completion:^{
        //
    }];
}

- (void)initialDownloadError:(void(^)())callback
{
    UIAlertController *alert = nil;
    alert = [UIAlertController alertControllerWithTitle:@""
                                                message:@"インストール時はコンテンツのダウンロードが必要です。\nネットワークを確認して再度ダウンロードしてください。"
                                         preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"もう一度ダウンロードする"
                                              style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction *action) {
                                                callback();
                                            }]];
    [[App getRootWebViewController] presentViewController:alert animated:YES completion:^{
        //
    }];
}

- (void)addHeader:(NSDictionary*)hedaer
{
    header_ = [[NSMutableDictionary alloc] initWithDictionary:hedaer];
}

- (void)removeHeader
{
    if(header_) {
        [header_ removeAllObjects];
    }
    header_ = nil;
}

@end

@interface CallbackObject() {
    downloadSucceeded succeed_;
    downloadFailed failed_;
    updateUI uiupdate_;
    NSURL *cachePath_;
    CALLBACK_TYPE type_;
}

@end

@implementation CallbackObject

@synthesize onProgress, etagCheck_;

- (void)setCallback:(downloadSucceeded)succeed failed:(downloadFailed)failed
{
    succeed_ = succeed;
    failed_ = failed;
    type_ = DOWNLOAD;
    etagCheck_ = NO;
}

- (void)setCallbackWithChechETag:(downloadSucceeded)succeed failed:(downloadFailed)failed
{
    succeed_ = succeed;
    failed_ = failed;
    type_ = DOWNLOAD;
    etagCheck_ = YES;
}


- (void)setCallback:(downloadSucceeded)succeed failed:(downloadFailed)failed cache:(NSURL*)cacheURL
{
    succeed_ = succeed;
    failed_ = failed;
    cachePath_ = cacheURL ? [NSURL URLWithString:[cacheURL absoluteString]] : nil;
    type_ = DOWNLOAD_CACHE;
    etagCheck_= NO;
}

- (void)setCallback:(updateUI)callback
{
    uiupdate_ = callback;
    type_ = UI_UPDATE;
    etagCheck_ = YES;
}

- (downloadSucceeded)getSucceed
{
    return succeed_;
}

- (downloadFailed)getFailed
{
    return failed_;
}

- (updateUI)getUpdateUI
{
    return uiupdate_;
}

- (NSURL*)getCachePath
{
    return cachePath_;
}

- (CALLBACK_TYPE)getType
{
    return type_;
}

- (BOOL)isETagCheck
{
    return etagCheck_;
}

@end
