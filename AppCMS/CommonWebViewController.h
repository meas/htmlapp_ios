//
//  CommonWebViewController.h
//  AppCMS
//
//  Created by 長島 伸光 on 2015/03/19.
//  Copyright (c) 2015年 MEAS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface CommonWebViewController : UIViewController <WKNavigationDelegate, WKUIDelegate> {
    IBOutlet UIToolbar *toolBar_;
    IBOutlet UIBarButtonItem *titleItem_;
    WKWebView *webView_;
}

- (IBAction)back:(id)sender;
- (void)setURL:(NSString*)url isSetting:(BOOL)isSetting title:(NSString*)title;
- (void)setLocalURL:(NSString*)url isSetting:(BOOL)isSetting title:(NSString*)title;

@end
